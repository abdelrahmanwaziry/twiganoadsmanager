import {StyleSheet, Platform} from 'react-native'

export const genericStyles = StyleSheet.create({
    mediaPlaceholder: {
        width: '100%',
        height: 250,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    cardStyle: {
        width: '100%',
        alignSelf: 'center',
        marginBottom: 1,
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: "#ddd",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.18,
        shadowRadius: 5.46,
        elevation: 1,
    },
    cardHeader: {
        padding: 10,
        flexWrap: 'wrap',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    cardHeaderText: {
        width: '84%',
        fontSize: 15,
        justifyContent: 'center',
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        fontWeight: 'bold',
        color: '#222',
        paddingLeft: 5,
    },
    cardHeaderIcon: {
        backgroundColor: '#ea1f4a',
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        marginRight: 5,
        borderRadius: 100,
    },
    cardFooter: {
        padding: 10,
        flexWrap: 'wrap',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center'
    },
    cardFooterText: {
        fontSize: 15,
        justifyContent: 'center',
        textAlign: 'center',
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        fontWeight: 'bold',
        color: '#222',
        paddingLeft: 5,
    },
    section: {
        width: '100%',
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff',
        marginBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#ddd",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.08,
        shadowRadius: 2.46,
        elevation: 1,
    },
    sectionHeader: {
        width: '100%',
        fontSize: 15,
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        fontWeight: 'bold',
        color: '#999',
        margin: 5,
        marginLeft: 15
    },
    inlineDivision: {
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%'
    },
    mapCardContainer: {
        position: 'absolute',
        width: '90%',
        bottom: 15,
        marginTop: 15,
        padding: 20,
        backgroundColor: '#fff',
        borderRadius: 8,
        alignSelf: 'center',
        shadowColor: "#222",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.08,
        shadowRadius: 2.46,
        elevation: 2,
    },
    cardInfoItem: {
        width: '80%', 
        paddingLeft: 15,
        marginTop: 10,
        fontSize: 15,
        color: '#666',
        fontWeight: 'bold'
    },
    mapSearchBar: {
        position: 'absolute', 
        width: '90%',
        top: 15,
        borderRadius: 4
    },
    stepsIndicatorContainer: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
    },
    stepIndicatorHeader: {
        backgroundColor: '#f2f2f2',
        paddingTop: 20,
        paddingBottom: 20
    },
    stepsText: {
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 5,
        fontWeight: 'bold',
        fontSize: 18,
        color: '#999'
    },
    backButtonStyle: {
        position: 'absolute',
        top: 5,
        left: 10,
        padding: 10
    },
    userCurrentBalance: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#444',
        marginLeft: 15,
        marginBottom: 10
    },
    coloredSection: {
      width: '45%',
      margin: 10,
      borderRadius: 8
    },
    notificationContainer: {
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'row',
    },
    unreadNotification: {
        backgroundColor: '#99c7ff36',
        borderLeftWidth: 5,
        borderLeftColor: '#99c7ff'
    },
    notificationTextContainer: {
        width: '70%',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        flexDirection: 'column',
        margin: 5,
    },
    notificationTitle: {
        fontSize: 14,
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        fontWeight: 'bold',
        color: '#324354',
        margin: 5,
        marginBottom: 2,
    },
    notificationMessage: {
        fontSize: 15,
        color: '#9CA5AE',
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        margin: 5,
        marginTop: 2
    },
    notificationThumb: {
        width: 55,
        height: 55,
        borderRadius: 5,
        resizeMode: 'cover'
    },
    bellCountContainer: {
        flexWrap: 'wrap', 
        alignSelf: 'flex-start',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 18,
        width: 18,
        marginTop: 2,
        marginRight: 95,
        backgroundColor: '#fff',
        borderRadius: 100,
    },
    bellCount: {
        fontWeight: 'bold',
        color: '#ea1f4a'
    },
    forwardButtonStyle: {
        position: 'absolute',
        top: -50,
        right: 10,
        padding: 10,
        zIndex: 200
    },
    infoButtonStyle: {
        position: 'absolute',
        top: 5,
        left: 10,
        zIndex: 200
    },
    modalHeader: {
        width: '100%',
        color: '#888',
        fontWeight: 'bold',
        backgroundColor: '#ddd',
        padding: 10,
        marginTop: 30,
        textAlign: 'center',
        justifyContent: 'center'
    }
})