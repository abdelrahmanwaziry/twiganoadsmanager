import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors } from './swiper.styles'

const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

const entryBorderRadius = 8;

export default StyleSheet.create({
    slideInnerContainer: {
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
        marginTop: 5,
        height: 130,
        justifyContent: 'center',
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        shadowColor: "#ddd",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.08,
        shadowRadius: 2.46,
        elevation: 1,
    },
    slideInnerContainerText: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#fff'
    },
    checkMark: {
        position: 'absolute',
        top: 5,
        right: 7
    },
});