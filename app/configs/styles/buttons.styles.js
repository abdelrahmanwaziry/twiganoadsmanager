import { StyleSheet, Platform, Dimensions } from 'react-native'

let width = Dimensions.get('window').width

export const buttonsStyles = StyleSheet.create({
    actionButton: {
        width: '100%',
        backgroundColor: '#01cf9e',
        position: 'absolute',
        bottom: 0,
        zIndex: -1
    },
    placeOrderButton: {
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#01cf9e',
        bottom: 0,
        borderRadius: 5,
        margin: 10
    },
    placeOrderButtonInnerStyle: {
        padding: 10
    },
    actionButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    actionButtonText: {
        color: '#fff',
        fontSize: 16,
        fontFamily: Platform.OS == 'ios' ? 'San Francisco' : 'Roboto',
        marginRight: 5,
        fontWeight: 'bold',
    },
    addButton: {
        width: '28%',
        borderRadius: 2,
        marginTop: 10,
        marginBottom: 2,
        padding: width * 3.5 / 100,
        backgroundColor: '#4a8bfc'
    },
    addButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    addButtonText: {
        color: '#fff',
        fontSize: 13,
        marginRight: 5,
        fontFamily: Platform.OS == 'ios' ? 'San Francisco' : 'Roboto',
        fontWeight: 'bold',
    },
    disabled: {
        opacity: .4
    },
    mapButtonStyle: {
        position: 'absolute',
        bottom: 5,
        backgroundColor: '#fff',
        right: 10,
        padding: 10
    },
    searchButtonStyle: {
        width: '20%',
        borderRadius: 2,
        marginTop: 5,
        marginBottom: 5,
        padding: width * 3.5 / 100,
        backgroundColor: '#4a8bfc'
    },
    genericButton: {
        alignSelf: 'center',
        borderRadius: 3,
        margin: 10
    },
    genericButtonInner: {
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
        padding: 8
    },
    genericButtonInnerContent: {
        color: '#fff',
        fontSize: 14,
        fontFamily: Platform.OS == 'ios' ? 'San Francisco' : 'Roboto',
        marginRight: 5,
        fontWeight: 'bold',
    },
    button: {
        padding: 12,
        margin: 3,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    }
})