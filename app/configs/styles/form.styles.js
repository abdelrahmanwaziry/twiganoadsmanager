import { StyleSheet, Platform, Dimensions } from 'react-native'

let width = Dimensions.get('window').width

export const formStyles = StyleSheet.create({
    formContainer: {
        width: '100%',
        height: '100%',
    },
    inputGroup: {
        flexWrap: 'wrap', 
        alignSelf: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        marginTop: 5
    },
    sectionGroup: {
        borderBottomColor: '#d8d8d8',
        borderBottomWidth: .5,
        width: '100%',
        flexWrap: 'wrap', 
        alignItems: 'flex-start',
        flexDirection:'row',
        marginTop: 0
    },
    inputGroupLabel: {
        marginTop: 10,
        marginBottom: 10,
        width: '100%',
        alignSelf: 'center',
        fontSize: 15,
        fontWeight: 'bold',
        color: '#aaa'
    },
    inputStyle: {
        padding: width * 3.5 / 100,
        paddingLeft: 10,
        borderRadius: 2,
        alignSelf: 'center',
        width: '100%',
        marginRight: 5,
        marginTop: 10,
        borderColor: '#d8d8d8',
        borderWidth: .5
    },
    withoutBorder: {
        borderColor: 'transparent',
        marginTop: 0,
    },
    actionButton: {
        width: '100%',
        backgroundColor: '#01cf9e',
        position: 'absolute',
        bottom: 0,
        height: '10%'
    },
    actionButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    actionButtonText: {
        color: '#fff',
        fontSize: 16,
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        marginRight: 5,
        fontWeight: 'bold',
    },
    dialogContainer: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: 'center'
    },
})