import store from 'react-native-simple-store'

export const StorageHandler = {
    getKey(key, callback){
        store.get(key).then((value) => {
            callback(value)
        }).catch(err => {
            callback(err)
        })
    },

    setKey(key, value){
        store.save(key, value)
    },

    deleteKey(key){
        store.delete(key)
    }
}