export default function validate(fieldName, value) {

  var pattern;

  // Checks input value persistance
  if(!value){
    return `${fieldName} is required`;
  }

  // Else, validate value based on fieldName
  
  else {
    if(fieldName === 'Products'){
      return value.length < 4 ? 'All product fields are required' : null
    }
    else if(fieldName === 'Duration'){
      return value < 15 ? 'Duration must be at least 15 days' : null
    }
    else {
      return null;
    }
  }
}