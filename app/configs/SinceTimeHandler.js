export function getSinceTime(endUNIX, startUNIX){
    var durationUNIX = endUNIX - startUNIX;
    var seconds, minutes, hours, days, weeks, monthes , years;
    days = Math.floor((durationUNIX / 3600) / 24);
    if(endUNIX && startUNIX){
        return days == 1 ? days + ' day ' : days + ' days '
    }
    else {
        return 'N/A'
    }
}