import store from 'react-native-simple-store'
import { networkHandler } from '../configs/NetworkHandler'

function generateRandomString(length){
    var result = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function placeOrder(token, amount){
    var orderBody = {
        delivery_needed: "false",
        merchant_id: 1286,
        amount_cents: Number(amount) * 100,
        currency: "EGP",
        merchant_order_id : generateRandomString(5),
        items: [],
        shipping_data: {
          apartment: "803", 
          email: "claudette09@exa.com", 
          floor: "42", 
          first_name: "Clifford", 
          street: "Ethan Land", 
          building: "8028", 
          phone_number: "+86(8)9135210487", 
          postal_code: "01898", 
          city: "Jaskolskiburgh", 
          country: "CR", 
          last_name: "Nicolas", 
          state: "Utah"
        }
    }
    networkHandler.createAcceptOrder(orderBody, token, (data) => {
        requestPayment(token, data.id)
    })
}

function requestPayment(token, orderID){
    var paymentBody = {
        "amount_cents": Number(this.state.balance) * 100, 
        "expiration": 36000, 
        "order_id": orderID ,
        "billing_data": {
          apartment: "803", 
          email: "claudette09@exa.com", 
          floor: "42", 
          first_name: "Clifford", 
          street: "Ethan Land", 
          building: "8028", 
          phone_number: "+86(8)9135210487", 
          shipping_method: "PKG", 
          postal_code: "01898", 
          city: "Jaskolskiburgh", 
          country: "CR", 
          last_name: "Nicolas", 
          state: "Utah"
        }, 
        currency: "EGP", 
        integration_id: 1636
    }
    networkHandler.requestPaymentKey(paymentBody, token, (data) => {
        checkout(data.token)
    })
}

function checkout(payment_token){
    store.get('cardInfo').then(cardInfo => {
    var expiryDate = cardInfo.expiry.split('/')
    var checkoutBody = {
        payment_token: payment_token,
        source: {
            identifier: cardInfo.number.replace(/\s+/g, ''),
            sourceholder_name: cardInfo.name,
            subtype: "CARD",
            expiry_month: expiryDate[0],
            expiry_year: expiryDate[1],
            cvn: cardInfo.cvc
        },
        billing: {
            first_name: "Clifford",
            last_name: "Nicolas",
            street: "Ethan Land",
            building: "8028",
            floor: "42",
            apartment: "803",
            city: "Jaskolskiburgh",
            state: "Utah",
            country: "CR",
            email: "claudette09@exa.com",
            phone_number: "+86(8)9135210487",
            postal_code: "01898"
        }
        
    }
    networkHandler.rechargeBalance(checkoutBody, (data) => {
        alert(JSON.stringify(data))
    })  
    })
}

export function rechargeBalance(amount){
    var body = {
        username: 'Twigano',
        password: '*n#5Q4%0xbuy',
        expiration: '36000'
    }
    networkHandler.acceptAuth(body, (data) => {
        placeOrder(data.token, amount)
    })
}
