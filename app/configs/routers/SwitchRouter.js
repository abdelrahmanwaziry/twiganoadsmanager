import { StackNavigator, SwitchNavigator } from 'react-navigation'
import { MainRouter } from './MainRouter';
import { AuthenticationRouter } from './AuthRouter'
import AppLoading from '../../views/AppLoading';
import BusinessAccount from '../../views/BusinessAccount';


const AppStack = StackNavigator({ Home: MainRouter});
const AuthStack = StackNavigator({ SignIn: AuthenticationRouter });

export default SwitchNavigator(
  {
    Loading: AppLoading,
    App: AppStack,
    Auth: AuthStack,
    Bussiness: BusinessAccount
  },
  {
    initialRouteName: 'Loading',
  }
);