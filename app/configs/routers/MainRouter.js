import React, {Component} from 'react'
import {Platform, Image} from 'react-native'
import { TabNavigator, TabBarBottom, StackNavigator } from 'react-navigation'
import { Ionicons } from '@expo/vector-icons'
import Home from './../../views/Home'
import Create from './../../views/Create'
import Campaign from './../../views/StandaloneCampaign'
import Traffic from './../../views/DriveTraffic'
import Awareness from './../../views/GrowAwareness'
import FeedProducts from './../../views/FeedProducts'
import TwiganoAds from './../../views/TwiganoAds'
import CreateAudience from './../../views/CreateAudience'
import AdPreview from './../../views/AdPreview'
import SetLocationView from './../../views/SetLocationView'
import UserSettings from './../../views/UserSettings'
import ProductSearch from './../../components/ProductSearch'
import UpdatePassword from './../../components/UpdatePassword'
import AudienceSets from '../../views/AudienceSets'
import AddContactInfo from '../../components/AddContactInfo';
import Login from '../../components/Login';
import Register from '../../components/Register'
import Notifications from '../../views/Notifications'
import homeactive from './../assets/media/navigation/home-active.png'
import homeinactive from './../assets/media/navigation/home-inactive.png'
import createactive from './../assets/media/navigation/create-active.png'
import createinactive from './../assets/media/navigation/create-inactive.png'
// import notificationsactive from './../assets/media/navigation/notifications-active.png'
// import notificationsinactive from './../assets/media/navigation/notifications-inactive.png'
import useractive from './../assets/media/navigation/user-active.png'
import userinactive from './../assets/media/navigation/user-inactive.png'


export const SettingsRouter = StackNavigator({
    UserSettings: {
        screen: UserSettings
    },
    UpdatePassword: {
        screen: UpdatePassword
    },
    AddContactInfo: {
        screen: AddContactInfo
    },
    CreateAudience: {
        screen: CreateAudience
    },
    AudienceSets: {
        screen: AudienceSets,
    }},{
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#ea1f4a',
                height: 45,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
        },
    }
)

export const AwarenessRouter = StackNavigator({
    Awareness: {
        screen: Awareness
    },
    
    AdPreview: {
        screen: AdPreview
    }}, {headerMode: 'none'})

export const TrafficRouter = StackNavigator({
    Traffic: {
        screen: Traffic
    },
    Campaign: {
        screen: Campaign
    },
    FeedProducts: {
        screen: FeedProducts
    },
    TwiganoAds: {
        screen: TwiganoAds
    },
    CreateAudience: {
        screen: CreateAudience
    },
    AdPreview: {
        screen: AdPreview
    },
    SetLocationView: {
        screen: SetLocationView
    },
    ProductSearch: {
        screen: ProductSearch
    }
    },
    {headerMode: 'none'}
)

export const CampaignCreate = StackNavigator({
        Create: {
            screen: Create,
        },
        Traffic: {
            screen: TrafficRouter
        },
        Awareness: {
            screen: Awareness
        }
    },
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#ea1f4a',
                height: 45,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
        },
    }
);

export const HomeRouter = StackNavigator({
    Home: {
        screen: Home,
        title: 'Home'
    },
    Campaign: {
        screen: Campaign
    },
    FeedProducts: {
        screen: FeedProducts
    },
    TwiganoAds: {
        screen: TwiganoAds
    },
    Awareness: {
        screen: Awareness
    },
    CreateAudience: {
        screen: CreateAudience
    },
    AdPreview: {
        screen: AdPreview
    },
    SetLocationView: {
        screen: SetLocationView
    }
    },
    {
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: '#ea1f4a',
                    height: 45,
                  },
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
            }
        },
    }
);

export const NotificationsRouter = StackNavigator({
    Notifications: {
        screen: Notifications,
    },
    Campaign: {
        screen: Campaign
    }
},
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#ea1f4a',
                height: 45,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
    }
);


export const MainRouter = TabNavigator({
        Home: {
            screen: HomeRouter,
            title: 'Home',
            navigationOptions: {
                tabBarLabel: 'Home',
            },
        },
        Create: {
            screen: CampaignCreate,
            navigationOptions: {
                tabBarLabel: 'Create',
            },
        },
        UserSettings: {
            screen: SettingsRouter,
            navigationOptions: {
                tabBarLabel: 'Profile',
            },
        }
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if(Platform.OS === 'ios'){
                    if (routeName === 'Home') {
                        return focused ? <Image source={homeactive} style={{width: 23, height: 23}}/> 
                        : <Image source={homeinactive} style={{width: 23, height: 23}}/>
                    } else if (routeName === 'Create') {
                        return focused ? <Image source={createactive} style={{width: 23, height: 23}}/> 
                        : <Image source={createinactive} style={{width: 23, height: 23}}/>
                    }
                    // else if (routeName === 'Notifications') {
                    //     return focused ? <Image source={notificationsactive} style={{width: 23, height: 23}}/> 
                    //     : <Image source={notificationsinactive} style={{width: 23, height: 23}}/>
                    // }
                    else if (routeName === 'UserSettings') {
                        return focused ? <Image source={useractive} style={{width: 23, height: 23}}/> 
                        : <Image source={userinactive} style={{width: 23, height: 23}}/>
                    }
                }
                else {
                    if (routeName === 'Home') {
                        iconName = `ios-home${focused ? '' : '-outline'}`;
                      } else if (routeName === 'Create') {
                        iconName = `ios-add-circle${focused ? '' : '-outline'}`;
                      }
                    //   else if (routeName === 'Notifications') {
                    //     iconName = `ios-notifications${focused ? '' : '-outline'}`;
                    //   }
                      else if (routeName === 'UserSettings') {
                        iconName = `ios-person${focused ? '' : '-outline'}`;
                      }
                      return <Ionicons name={iconName} size={25} color={tintColor}/>;
                }
              },
          }),
          tabBarComponent: TabBarBottom,
          backgroundColor: '#ddd',
          tabBarPosition: 'bottom',
          tabBarOptions: {
            activeTintColor: '#d80746',
            inactiveTintColor: '#808080',
          },
          animationEnabled: true,
          swipeEnabled: true,
        }
  );


  