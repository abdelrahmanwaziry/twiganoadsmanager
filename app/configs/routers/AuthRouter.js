import { StackNavigator } from 'react-navigation'
import Login from './../../components/Login'
import Register from './../../components/Register'
import BusinessAccountForm from './../../components/BusinessAccountForm'
import BusinessAccount from '../../views/BusinessAccount'
import { MainRouter } from './MainRouter';


export const AuthenticationRouter = StackNavigator(
    {
      Login: {
        screen: Login,
      },
      Register: {
        screen: Register,
      },
      BusinessAccount: {
        screen: BusinessAccount
      },
      BusinessAccountForm: {
        screen: BusinessAccountForm
      }
    },{headerMode: 'none'}
)