var baseURL = 'https://dev.twigano.com/api/'

export const networkHandler = {
    loginHandler(body, callback){
        fetch(baseURL + 'v1/login', {
			method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    createSession(body, token, id, callback){
        fetch(baseURL + `v1/users/${id}/sessions`, {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    registerHandler(body, callback){
        fetch(baseURL + 'v1/register', {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    updateAccount(body, token, callback){
        fetch(baseURL + 'v1/profile', {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    fetchProfile(token, callback){
        fetch(baseURL + 'v1/profile', {
            headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    logoutHandler(body, token, callback){
        fetch(baseURL + 'v1/logout', {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    acceptAuth(body, callback){
        fetch('https://accept.paymobsolutions.com/api/auth/tokens', {
			method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    createAcceptOrder(body, token, callback){
        fetch('https://accept.paymobsolutions.com/api/ecommerce/orders?token=' + token, {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
			}
            }).then(response => {
                return response.json()
            }).then(data => {
                callback(data)
            }).catch(err => {
        })
    },
    requestPaymentKey(body, token, callback){
        fetch(`https://accept.paymobsolutions.com/api/acceptance/payment_keys?token=${token}`, {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
            }).then(response => {
                return response.json()
            }).then(data => {
                callback(data)
            }).catch(err => {
        })
    },
    rechargeBalance(body, token, callback){
        fetch(baseURL + 'v1/balance/pay', {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
            }).then(response => {
                return response.json()
            }).then(data => {
                callback(data)
            }).catch(err => {
                callback(err)
        })
    },
    listAudience(token, callback){
        fetch(baseURL + 'v1/audience', {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        })
    },
    deleteAudience(token, id, callback){
        fetch(baseURL + `v1/audience/${id}`, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        })
    },
    createAudience(body, token, callback){
        fetch(baseURL + 'v1/audience', {
			method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
        })
    },
    updateAudience(body, id ,token, callback){
        fetch(baseURL + `v1/audience/${id}`, {
			method: 'put',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
        })
    },
    attachAudience(body, token, id, callback){
        fetch(baseURL + `v1/campaigns/${id}/audience`, {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(data)
        })
    },
    fetchRecommendationsAds(token, callback){
        fetch(baseURL + 'v1/recommendations/ads', {
			headers: {
				'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(data)
        })
    },
    fetchCampaign(token, id, callback){
        fetch(baseURL + `v1/campaigns/${id}`, {
			headers: {
				'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    createCampaign(body, token, callback){
        fetch(baseURL + 'v1/campaigns', {
			method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    listCampaigns(token, callback){
        fetch(baseURL + 'v1/campaigns', {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        })
    },
    listBanners(token, callback){
        fetch(baseURL + 'v1/banners/requests', {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        })
    },
    listCreatives(token, id, callback){
        fetch(baseURL + `v1/campaigns/${id}/creatives`, {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        }) 
    },
    deleteCampaign(token, id, callback){
        fetch(baseURL + `v1/campaigns/${id}`, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        })
    },
    editCampaign(body, token, id, callback){
        fetch(baseURL + `v1/campaigns/${id}`, {
			method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    productSearch(keyword, token, callback){
        fetch(baseURL + `v1/search/products/${keyword}`, {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        })
    },
    createBudgetAndSchedule(body, token, id ,callback){
        fetch(baseURL + `v1/campaigns/${id}/budget-schedule`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            method: 'post',
			body: JSON.stringify(body),
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    createCreative(body, token, id ,callback){
        fetch(baseURL + `v1/campaigns/${id}/creatives`, {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            method: 'post',
			body: body,
        }).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    createProductsCreative(body, token, id, callback){
        fetch(baseURL + `v1/campaigns/${id}/creatives`, {
			method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    createBannerCreative(body, token, callback){
        fetch(baseURL + 'v1/banners/requests', {
            method: 'post',
			body: body,
			headers: {
				'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    },
    createBusinessAccount(body, token, callback){
        fetch(baseURL + 'v1/business-accounts', {
            method: 'post',
			body: JSON.stringify(body),
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
			}
		}).then(response => {
            return response.json()
        }).then(data => {
            callback(data)
        }).catch(err => {
            callback(err)
        })
    }
}