export function formatNumber(value){
    if((value / 1000) < 1){
        return value
    }
    else if((value / 1000) > 1) {
        return (value / 1000) + 'k'
    }
}