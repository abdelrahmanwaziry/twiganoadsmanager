import React, {Component} from 'react'
import { View, Text, Alert, ScrollView, TextInput } from 'react-native'
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input-fullpage"
import PopupDialog, { DialogTitle, DialogButton } from 'react-native-popup-dialog'
import TwiganoAd from './../components/templates/TwiganoAdTemplate'
import { formStyles } from '../configs/styles/form.styles'
import { genericStyles } from '../configs/styles/generic.styles'
import { buttonsStyles } from '../configs/styles/buttons.styles'
import GenericButton from './../components/GenericButton'
import store from 'react-native-simple-store'
import { networkHandler } from '../configs/NetworkHandler'


export default class AdPreview extends Component {
    constructor(props){
        super(props)
        this.state = {
            ad_details: [],
            campaign_details: [],
            loaded: true,
            cardInfo: {},
            balance: 0,
            new_balance: 0,
            showBalanceDialog: false,
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    static navigationOptions = {
        title: 'Preview Ad',
        headerTitleStyle: {
            textAlign: 'center',
            alignSelf:'center',
            fontWeight: 'bold'
        },
        headerRight: (<View></View>),
        tabBarVisible: false, 
        swipeEnabled: false,
    }

    componentDidMount(){
        
        const { params } = this.props.navigation.state;
        const info = params ? params.info : null
        const campaign = params ? params.campaign : null

        let details = [
            {name: 'Audience', value: info.selected_audience.name , key: 'audience'},
            {name: 'Budget', value: info.life_time_budget, key: 'budget'},
            {name: 'Daily Budget', value: info.daily_budget, key: 'daily_budget'},
            {name: 'Cost Per Click', value: info.billing_event_budget, key: 'cpc'},
            {name: 'Schedule', value: info.start_time + ' - ' + info.end_time , key: 'schedule'
            }
        ]

        let campaign_details = [
            {name: 'Name', value: campaign.name, key: 'name'},
            {name: 'Objective', value: campaign.objective , key: 'objective'},
        ]
        
        this.setState({
            ad_details: details,
            campaign_details: campaign_details
        })

        store.get('cardInfo').then(cardInfo => {
            if(cardInfo){
                this.setState({
                    cardInfo: cardInfo
                })
            }
        })

        store.get('balance').then(balance => {
            this.setState({
                balance: balance
            })
        })
        
    }

    updateBalance(value){
        this.setState(prevState => ({
            balance: Number(prevState.balance) + Number(value)
        }), () => {
            store.save('balance', this.state.balance)
            this.handleSubmit()
        })
    }
    
    checkout(amount){
        this.setState({loaded: false})
        if(this.state.cardInfo){
            var expiryDate = this.state.cardInfo.expiry.split('/')
            var checkoutBody = {
                identifier: this.state.cardInfo.number.replace(/\s+/g, ''),
                sourceholder_name: this.state.cardInfo.name,
                expiry_month: expiryDate[0],
                expiry_year: expiryDate[1],
                cvn: this.state.cardInfo.cvc,
                amount: parseInt(amount) * 100
            }
            store.get('access_token').then((token) => {
                networkHandler.rechargeBalance(checkoutBody, token, (data) => {
                    if(data.data){
                        this.updateBalance(amount)
                        this.setState({loaded: true})
                    }
                    else {
                        var errorMessage = data.payment.length > 1 ? 'Invalid Credit Card' : 'Payment Declined'
                        alert(errorMessage);
                        this.setState({loaded: true})
                    }
                }) 
            })
        } 
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    handleActionTitle(type){
        var splittedType = type.split('_');
        return this.capitalizeFirstLetter(splittedType[0]) + ' ' + this.capitalizeFirstLetter(splittedType[1])
    }  

    handleSubmit(){
        
        this.setState({loaded: false})
        
        const { params } = this.props.navigation.state;
        const info = params ? params.info : null
        const campaign = params ? params.campaign : null
        const content = params ? params.content : null
        const coords = params ? params.coords : null
        let formData = new FormData()

        var budgetBody = {
            _method: 'PUT', 
            daily_budget: info.daily_budget,
            life_time_budget: info.life_time_budget,
            billing_event_budget: info.billing_event_budget,
            start_time: info.start_time,
            end_time: info.end_time
        }

        var campaignBody = {
            name: campaign.name,
            objective: campaign.objective
        }

        if(content.type === 'media'){
            
            let localUri = content.media;
            let filename = localUri.split('/').pop();
            let match = /\.(\w+)$/.exec(filename);
            let type = match ? `image/${match[1]}` : `image`

            formData.append("type", content.type);
            content.type == 'media' && formData.append('media', {
                uri: localUri,
                name: filename,
                type: type
            });

            formData.append('call_to_action', content.call_to_action);
            formData.append('action_url', content.action_url && content.action_url);
            content.call_to_action == 'get_directions' && formData.append('latitude', 
            coords.latitude && coords.latitude.toFixed(5));
            content.call_to_action == 'get_directions' && formData.append('longitude', 
            coords.longitude && coords.longitude.toFixed(5));
            formData.append('caption', content.caption);
            formData.append('name', content.name);
            formData.append('status', 'pending');
        }

        var attachAudienceBody = {
            _method: 'PUT',
            audience: info.selected_audience.id
        }

        var feedProductsBody = {
            type: content.type,
            products: content.products,
            name: content.name ? content.name : 'New Creative',
            status: 'published'
        }

        if(this.state.balance >= info.life_time_budget){
            store.get('access_token').then((token) => {
                if(!campaign.id) {
                    networkHandler.createCampaign(campaignBody, token, (campaign) => {
                        networkHandler.createBudgetAndSchedule(budgetBody, token, campaign.id, (data) => {
                            networkHandler.attachAudience(attachAudienceBody, token, campaign.id, (data) => {
                                console.log(data)
                            })
                        })
                        if(content.type == 'media'){
                            networkHandler.createCreative(formData, token, campaign.id, (data) => {
                                if(data.data){
                                    this.props.navigation.navigate('Home')
                                }
                                else {
                                    alert(JSON.stringify(data)) 
                                }
                                this.setState({loaded: true})
                            })
                        }
                        else {
                            networkHandler.createProductsCreative(feedProductsBody, token, campaign.id, (data) => {
                                if(data.data){
                                    this.props.navigation.navigate('Home')
                                }
                                else {
                                    alert(JSON.stringify(data)) 
                                }
                                this.setState({loaded: true})
                            })
                        }
                    })
                }
                else {
                    networkHandler.createBudgetAndSchedule(budgetBody, token, campaign.id, (data) => {
                        networkHandler.attachAudience(attachAudienceBody, token, campaign.id, (data) => {
                            console.log(data)
                        })
                    })
                    if(content.type === 'media'){
                        networkHandler.createCreative(formData, token, campaign.id, (data) => {
                            if(data.data){
                                this.props.navigation.navigate('Home')
                            }
                            else {
                                alert(JSON.stringify(data)) 
                            }
                            this.setState({loaded: true})
                        })
                    }
                    else {
                        networkHandler.createProductsCreative(feedProductsBody, token, campaign.id, (data) => {
                            if(data.data){
                                this.props.navigation.navigate('Home')
                            }
                            else {
                                alert(JSON.stringify(data)) 
                            }
                            this.setState({loaded: true})
                        })
                    }
                }
            }) 
        }
        else {
            if(!this.state.cardInfo.number){
                this.popupDialog.show()
            }
            else {
                this.setState({
                    showBalanceDialog: true
                }, () => {
                    this.balanceDialog.show()
                })
            }
            this.setState({loaded: true})
        }
        
    }
    
    render(){    

        const { params } = this.props.navigation.state;
        const content = params ? params.content : null
        const coords = params ? params.coords : null
        
        return(
            <ScrollView style={{flex: 1}}>
                <PopupDialog
                    containerStyle={{ 
                        flex: 1, 
                        height: '100%', 
                        justifyContent: 'center',
                        marginTop: -30,
                        elevation: 4
                    }}
                    width={.9}
                    height={500}
                    dialogTitle={<DialogTitle title='Set Billing Info' />}
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    actions={[
                    <DialogButton
                        textContainerStyle={{paddingHorizontal: 10, paddingVertical: 8}}
                        textStyle={{color: '#fff', fontSize: 14}}
                        buttonStyle={[buttonsStyles.genericButton, {
                            backgroundColor: '#4d7bf3',
                            width: '80%',
                        }]}
                        text='Next'
                        onPress={() => {
                            this.popupDialog.dismiss()
                            store.save('cardInfo', cardInfo)
                            if(this.state.showBalanceDialog){
                                setTimeout(() => {
                                    this.balanceDialog.show()
                                }, 200)
                            }
                        }}
                        key="button-1"
                    />,
                    ]}
                    >
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <CreditCardInput
                            requiresName={true}
                            ref={(cardInput) => { this.cardInput = cardInput; }}
                            allowScroll={true}
                            onChange={(form) => {
                                this.appendCardValues(form.values)
                            }}
                        />
                    </View>
                </PopupDialog>
                <PopupDialog
                    containerStyle={{marginTop: -60, elevation: 4}}
                    width={.9}
                    height={250}
                    dialogTitle={<DialogTitle title='Recharge Your Balance' />}
                    ref={(balanceDialog) => { this.balanceDialog = balanceDialog; }}
                    actions={[
                    <DialogButton
                        textContainerStyle={{paddingHorizontal: 10, paddingVertical: 8}}
                        textStyle={{color: '#fff', fontSize: 14}}
                        buttonStyle={[buttonsStyles.genericButton, {
                            backgroundColor: '#4d7bf3',
                            width: '80%',
                        }]}
                        text='Submit'
                        onPress={() => {
                            this.checkout(this.state.new_balance)
                            this.balanceDialog.dismiss()
                        }}
                        key="button-1"
                    />,
                    ]}
                    >
                    <View style={{flex: 1}}>
                        <Text style={[genericStyles.sectionHeader, {color: '#555'}]}>
                            You don't have sufficient balance to run this campaign
                        </Text>
                        <TextInput 
                            placeholder='Balance'
                            onChangeText={(new_balance) => this.setState({new_balance})}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            keyboardType='numeric'
                            style={[formStyles.inputStyle, {width: '90%', backgroundColor: '#fff'}]}>
                        </TextInput>
                    </View>
                </PopupDialog> 
                <View style={genericStyles.section}>
                    <Text style={genericStyles.sectionHeader}>
                        Ad Preview
                    </Text>
                    {content.type == 'media' && <TwiganoAd 
                        actionTitle={content.call_to_action && this.handleActionTitle(content.call_to_action)} 
                        coords={content.call_to_action == 'get_directions' && 
                        coords.latitude + ',' + coords.longitude}
                        content={content}
                    />}
                    {/* <FeedProducts content={null}/> */}
                </View>
                <View style={genericStyles.section}>
                    <Text style={genericStyles.sectionHeader}>
                        Ad Details
                    </Text>
                    {this.state.ad_details.map((entry, index) =>
                        <View key={entry.key} style={[formStyles.inputGroup, formStyles.sectionGroup ,
                        {flexDirection: 'row'}]}>
                            <Text style={[formStyles.inputStyle, formStyles.withoutBorder ,{
                                    alignSelf: 'flex-start',
                                    width: '25%'
                                }]}>
                                {entry.name}
                            </Text>
                            <Text
                                style={[formStyles.inputStyle, formStyles.withoutBorder ,{
                                    alignSelf: 'flex-end',
                                    textAlign: 'right',
                                    width: '65%'
                                }]}
                            >
                            {entry.value}
                            </Text>
                            <GenericButton
                                disabled={false}
                                pressHandler={this.props.backButtonHandler}
                                hasIcon={true}
                                loaded={true}
                                underlayColor='transparent'
                                style={{marginRight: 10}}
                                icon={{iconName: 'md-create', size: 20, color: '#ddd'}}
                            />
                        </View>
                    )}
                </View>
                <View style={genericStyles.section}>
                    <Text style={genericStyles.sectionHeader}>
                        Campaign Details
                    </Text>
                    {this.state.campaign_details.map((entry, index) =>
                        <View key={entry.key} style={[formStyles.inputGroup, formStyles.sectionGroup ,
                        {flexDirection: 'row'}]}>
                            <Text style={[formStyles.inputStyle, formStyles.withoutBorder ,{
                                    alignSelf: 'flex-start',
                                    width: '25%'
                                }]}>
                                {entry.name}
                            </Text>
                            <Text
                                style={[formStyles.inputStyle, formStyles.withoutBorder ,{
                                    alignSelf: 'flex-end',
                                    textAlign: 'right',
                                    width: '65%'
                                }]}
                            >
                            {entry.value}
                            </Text>
                            <GenericButton
                                disabled={false}
                                pressHandler={this.props.backButtonHandler}
                                hasIcon={true}
                                loaded={true}
                                underlayColor='transparent'
                                style={{marginRight: 10}}
                                icon={{iconName: 'md-create', size: 20, color: '#ddd'}}
                            />
                        </View>
                    )}
                </View>
                <GenericButton
                   pressHandler={() => {
                        this.handleSubmit()
                   }}
                   loaded={this.state.loaded}
                   style={[buttonsStyles.placeOrderButton]}
                   innerStyle={[buttonsStyles.actionButtonInnerStyle, buttonsStyles.placeOrderButtonInnerStyle]}
                   textStyle={[buttonsStyles.actionButtonText, {fontWeight: 'normal'}]}
                   title='Place Order'
                   underlayColor="#008263"
                />
            </ScrollView>
        )
    }
}
