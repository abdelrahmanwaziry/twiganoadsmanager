import React, {Component} from 'react'
import { StackNavigator } from 'react-navigation'
import { AuthenticationRouter } from './../configs/routers/AuthRouter'

export default class Authentication extends Component {
  render(){
      return(
          <AuthenticationRouter/>
      )
  }
}