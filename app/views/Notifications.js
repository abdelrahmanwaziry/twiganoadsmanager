import React, {Component} from 'react'
import {ScrollView, RefreshControl, View, Text} from 'react-native'
import EmptyState from '../components/EmptyState'
import store from 'react-native-simple-store'
import Moment from 'moment'
import SingleNotification from '../components/SingleNotification'
import { genericStyles } from '../configs/styles/generic.styles'

export default class Notifications extends Component {
    static navigationOptions = ({navigation}) => {
        const { state, setParams, navigate } = navigation;
        const params = state.params || {};
        return {
            title: 'Notifications',
            headerTitleStyle: {
                textAlign: 'center',
                alignSelf:'center'
            },
            headerRight: (
                <View style={genericStyles.bellCountContainer}>
                    <Text style={genericStyles.bellCount}>{params.bell_count ? params.bell_count : 0}</Text>
                </View>
            ),
            headerLeft: (<View></View>)
        }
    }

    constructor(props){
        super(props)
        this.state = {
            data: [],
            refreshing: false,
            loaded: false,
        }
    }

    _onRefresh() {
        this.setState({refreshing: true});
        // this.fetchNotifications('refresh')
    }

    fetchNotifications(resource){
        store.get('user').then(user => {
            var bellCount = 0;
            var notifications = [
                {
                    object: user,
                    action: 'campaign needs attention',
                    read: false,
                    subject: {
                        campaign_id: 1,
                        thumbnail: 'https://source.unsplash.com/random/200x200',
                        message: 'Recharge your balance to keep the campaign running',
                        created_at: Moment()
                    }
                },
                {
                    object: user,
                    read: true,
                    action: 'campaign needs attention',
                    subject: {
                        campaign_id: 1,
                        thumbnail: 'https://source.unsplash.com/collection/1538150/200x200',
                        message: 'Recharge your balance to keep the campaign running',
                        created_at: Moment()
                    }
                }
                ,{
                    object: user,
                    read: false,
                    action: 'campaign needs attention',
                    subject: {
                        campaign_id: 1,
                        thumbnail: 'https://source.unsplash.com/collection/504554/200x200',
                        message: 'Recharge your balance to keep the campaign running',
                        created_at: Moment()
                    }
                }
            ]
            this.setState({
                data: notifications
            })
            notifications.forEach(notification => {
                if(!notification.read){
                    bellCount++
                }
            })
            this.props.navigation.setParams({
                bell_count: bellCount
            })
            if(resource == 'refresh'){
                this.setState({
                    refreshing: false
                })
            }
        })
    }

    componentDidMount(){
        // this.fetchNotifications('lifecycle')
    }

    render(){
        return(
            <ScrollView
                style={{flex: 1}}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh.bind(this)}
                    />
                }
            >
                {
                    this.state.data.length > 1 ? 
                    this.state.data.map((notification, index) => {
                        return( 
                            <SingleNotification key={index} notification={notification}/>
                        )
                    })
                    : <View style={{paddingTop: 200}}><EmptyState/></View>
                }
            </ScrollView>
        )
    }
}