import React, { Component } from 'react' 
import { View, Text, Alert } from 'react-native'
import StepsIndicator from './../components/GenericFormContainer'
import GrowAwarenessContentForm from './../components/GrowAwarenessContentForm'
import GrowAwarenessInfoForm from './../components/GrowAwarenessInfoForm'
import store from 'react-native-simple-store'
import validate from '../configs/ValidationHandler'
import { networkHandler } from '../configs/NetworkHandler';

export default class GrowAwareness extends Component {
    constructor(props){
        super(props)
        this.state = {
            currentStep: null,
            content: {},
            info: {},
        }
    }

    static navigationOptions = {
        tabBarVisible: false,
        swipeEnabled: false,
        header: null 
    }

    validateContent(state){
        var errorMessages = [
            validate('Name', state.name),
            validate('Banner URL', state.url),
            validate('Media', state.media)
        ]
        this.contentForm.showErrorMessage(errorMessages)

    }

    shouldComponentUpdate(){
        return false
    }

    render(){
        
        const { params } = this.props.navigation.state;
        const campaign = params ? params.campaign : null

        return(
            <View>
                <StepsIndicator
                    steps={2}
                    formTitle='Grow Awareness'
                    labels={['Content', 'Info']}
                    backButtonHandler={() => {
                        Alert.alert(
                            'Unsaved Changes',
                            'Unsaved Changes will be lost ?',
                            [
                                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                {text: 'OK', onPress: () => this.props.navigation.goBack()},
                            ],
                            { cancelable: false }
                        )
                    }}
                    getDatafromNavigate={() => {
                        this.setState({
                            content: {
                                media: this.contentForm.state.media,
                                media_type: this.contentForm.state.media_type,
                                action_url: this.contentForm.state.url,
                                name: this.contentForm.state.name
                            }
                        })
                        this.validateContent(this.contentForm.state)
                    }}
                    pages={[<GrowAwarenessContentForm 
                        ref={(contentForm) => {this.contentForm = contentForm}}
                        sendData={(content) => {
                            this.setState({
                                content: content
                            })
                        }}
                        sendErrors={(errors) => {
                            this.contentForm.showErrorMessage(errors)
                        }}
                        disableSubmitButton={() => {
                            this.info.disableButton()
                        }}
                        enableSubmitButton={() => {
                            this.info.enableButton()
                        }}
                        sendMedia={(cover) => {
                            this.info.setCover(cover)
                        }}
                    />, 
                    <GrowAwarenessInfoForm
                        ref={(info) => {this.info = info}}
                        loaded={this.state.loaded}
                        sendData={(info) => {
                            this.info.setLoadingState(false)
                            this.setState({
                                info: info,
                            }, () => {
                                let formData = new FormData()
                                let localUri = this.state.content.media;
                                let filename = localUri.split('/').pop();
                                let match = /\.(\w+)$/.exec(filename);
                                let type = match ? `image/${match[1]}` : `image`
                                
                                formData.append('media', {
                                    uri: localUri,
                                    name: filename,
                                    type: type
                                })
                                formData.append('name', this.state.content.name)
                                formData.append('target_url', this.state.content.action_url)
                                formData.append('duration', this.state.info.duration)
                                formData.append('slot', this.state.info.slot)
                                formData.append('bid', this.state.info.budget)
                    
                                store.get('access_token').then((token) => {
                                    networkHandler.createBannerCreative(formData, token, (data) => {
                                        if(data.data.id){
                                            this.props.navigation.navigate('Home', {
                                                refresh: true
                                            })
                                        }
                                        else {alert(JSON.stringify(data))}
                                        this.info.setLoadingState(true)
                                    })
                                })
                            })
                        }}
                    />]}
                />
            </View>
        )
    }
}
