import React, { Component } from 'react' 
import { View, 
        Text, 
        TextInput, 
        StyleSheet, 
        TouchableHighlight, 
        Platform, 
        Dimensions, 
        Alert,
        ImageBackground } from 'react-native'
import NextButton from './../components/GenericButton'
import { Ionicons } from '@expo/vector-icons'
import TwiganoAdsCover from './../configs/assets/media/TwiganoAds.jpg'
import FeedProductsCover from './../configs/assets/media/FeedProducts.jpg'
import { formStyles } from '../configs/styles/form.styles'
import GenericButton from './../components/GenericButton'
import { genericStyles } from '../configs/styles/generic.styles'

export default class DriveTraffic extends Component {
    constructor(props){
        super(props)
        this.state = {
            currentStep: null,
            title: '',
            campaign_type: 'feed',
            loaded: true,
        }
    }

    static navigationOptions = ({navigation}) => { 
        return {
            title: 'Drive Traffic',
            headerTitleStyle: {
                textAlign: 'center',
                alignSelf:'center',
            },
            headerLeft: (
                <Ionicons style={{marginLeft: 15, paddingHorizontal: 5}} onPress={() => {
                    navigation.navigate('Create')
                }} name='ios-arrow-back' size={28} color='#fff'/>
            ),
            headerRight: (<View></View>),
            tabBarVisible: false, 
            swipeEnabled: false,
        }
    }

    submitHandler(){
            this.setState({loaded: false})
            var body = {
                name: this.state.title,
                objective: 'traffic'
            }
            if(this.state.campaign_type == 'feed'){
                this.props.navigation.navigate('FeedProducts', {
                    campaign: body
                })
            } 
            else {
                this.props.navigation.navigate('TwiganoAds', {
                    campaign: body
                })
            }
    }

    selectType(type){
        this.setState({
            campaign_type: type
        }, () => {
            if(this.props.isUpdate){
                this.props.selectType(this.state.campaign_type)
            }
        })
    }

    renderInfoButton(objective){
        var message = objective == 'feed' ? 'A group of chosen products located on the timeline of your target audience where they can view and add to their collections. It leads the users directly to your website when clicked.' 
        : 'Ads that appear on the timeline of your target audience with any "call to action" you prefer.'
        return (
            <GenericButton
                disabled={false}
                pressHandler={() => {
                    Alert.alert('', message)
                }}
                hasIcon={true}
                loaded={true}
                underlayColor='transparent'
                style={genericStyles.infoButtonStyle}
                icon={{iconName: 'ios-information-circle', size: 26, color: '#fff'}}
            />
        )
    }

    render(){
        return(
            <View style={{alignItems: 'center', height: '100%'}}>
                {!this.props.isUpdate && <TextInput 
                    placeholder='Campaign Name'
                    onChangeText={(title) => this.setState({title})}
                    underlineColorAndroid='transparent'
                    placeholderTextColor='#999'
                    style={styles.inputStyle}>
                </TextInput>}    
                <TouchableHighlight 
                    style={styles.typeSelector}
                    underlayColor='transparent'
                    onPress={(e) => {
                        e.preventDefault()
                        this.selectType('feed')
                    }}
                >
                    <ImageBackground style={[formStyles.formContainer, styles.typeSelectorContent]}
                     resizeMode='cover' source={FeedProductsCover}>
                        <View>
                            <Text style={styles.typeSelectorText}>
                                Feed Products
                            </Text>
                        </View>
                        {this.state.campaign_type == 'feed' && 
                            <Ionicons style={styles.checkMark} 
                            name='ios-checkmark-circle' size={25} 
                        color='#fff'/>}
                        {this.renderInfoButton(this.state.campaign_type)}
                    </ImageBackground>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor='transparent'
                    style={styles.typeSelector}
                    onPress={(e) => {
                        e.preventDefault()
                        this.selectType('ads')
                    }}
                >
                    <ImageBackground style={[formStyles.formContainer, styles.typeSelectorContent]} 
                    resizeMode='cover' source={TwiganoAdsCover}>
                        <View>
                            <Text style={styles.typeSelectorText}>
                                Twigano Ads
                            </Text>
                        </View>
                        {this.state.campaign_type == 'ads' && 
                            <Ionicons style={styles.checkMark} 
                            name='ios-checkmark-circle' size={25} 
                        color='#fff'/>}
                        {this.renderInfoButton(this.state.campaign_type)}
                    </ImageBackground>
                </TouchableHighlight>
                {!this.props.isUpdate && <NextButton 
                    disabled={!this.state.title}
                    pressHandler={() => {
                        this.submitHandler()
                    }}
                    style={[styles.actionButton, !this.state.title && styles.disabled]} 
                    innerStyle={styles.actionButtonInnerStyle}
                    textStyle={styles.actionButtonText}
                    underlayColor="#177ab7"
                    title='Next'
                    loaded={true}
                    hasIcon={true}
                    icon={{iconName: 'md-arrow-round-forward', size: 16, color: '#fff'}}
                />}
            </View>
        )
    }
}

let width = Dimensions.get('window').width

const styles = StyleSheet.create({
    inputStyle: {
        padding: width * 3.5 / 100,
        paddingLeft: 10,
        borderRadius: 5,
        alignSelf: 'center',
        width: '90%',
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        marginTop: 10,
        marginBottom: 10,
        color: '#222',
        backgroundColor: '#fff',
        borderColor: '#999',
        borderWidth: .5
    }, 
    typeSelector: {
        height: '35%',
        width: '90%',
        borderRadius: 5,
        marginTop: 5,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        shadowColor: "#ddd",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.08,
        shadowRadius: 2.46,
        elevation: 1,
    },
    coverImage: {
        resizeMode: 'cover',    
        alignSelf: 'center'
    },
    checkMark: {
        position: 'absolute',
        top: 5,
        right: 7
    },
    typeSelectorContent: {
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
        justifyContent: 'center',
    },
    typeSelectorText: {
        textAlign: 'center',
        fontSize: 22,
        color: '#fff',
        fontWeight: 'bold',
        marginLeft: 10
    },
    actionButton: {
        width: '100%',
        backgroundColor: '#1AA3F6',
        position: 'absolute',
        bottom: 0,
        height: '10%'
    },
    disabled: {
        backgroundColor: '#1aa3f642'
    },
    actionButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    actionButtonText: {
        color: '#fff',
        fontSize: 16,
        marginRight: 5,
        fontFamily: Platform.os == 'ios' ? 'San Francisco' : 'Roboto',
        fontWeight: 'bold',
    }
})