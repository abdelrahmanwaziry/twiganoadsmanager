import React, {Component} from 'react'
import {Text, View, Keyboard, Slider, TextInput, Dimensions, Platform, ActivityIndicator} from 'react-native'
import { MapView, Permissions, Location } from 'expo'
import { Ionicons, Octicons } from '@expo/vector-icons'
import { formStyles } from '../configs/styles/form.styles'
import { buttonsStyles } from '../configs/styles/buttons.styles'
import {genericStyles} from '../configs/styles/generic.styles'
import GenericButton from './../components/GenericButton'

let width = Dimensions.get('window').width

export default class SetLocationView extends Component {
    constructor(props) {
        super(props);
        this.state = {
          region: {},
          coordinate: {},
          loaded: false,
          address: '',
          currentAddress: '',
          radius: 500,
        }
    }

    static navigationOptions = {
        title: 'Set Location',
        headerTitleStyle: {
            textAlign: 'center',
            alignSelf:'center',
        },
        tabBarVisible: false, 
        swipeEnabled: false,
    }

    componentDidMount(){
        this.getUserLocation()
        Permissions.askAsync(Permissions.LOCATION);
    }
    
    onMapPress(e){
        this.setState({
            coordinate: e.nativeEvent.coordinate,
        }, () => {
            this._attemptReverseGeocodeAsync()
        })
    }

    getUserLocation(){
		fetch('http://ip-api.com/json')
		.then(response => {
			return response.json()
		}).then(data => {
			this.setState({
                loaded: true,
				region: {
                    latitude: data.lat,
                    longitude: data.lon,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0922,
                },
                coordinate: {
                    latitude: data.lat,
				    longitude: data.lon,
                }
			})
		}).catch(err => {
			console.log(err)
		})
    }

    _attemptReverseGeocodeAsync = async () => {
        try {
            let result = await Location.reverseGeocodeAsync(this.state.coordinate)
            this.setState({
                currentAddress: result[0].name
            })
        } catch (error) {
            
        }
    } 
    
    _attemptGeocodeAsync = async () => {
        this.setState({ inProgress: true, error: null });
        try {
          let result = await Location.geocodeAsync(this.state.address);
          this.setState({
            region: {
                latitude: result[0].latitude,
                longitude: result[0].longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0922,
            },
            coordinate: {
                latitude: result[0].latitude,
                longitude: result[0].longitude
            }
          }, () => {
            this.map.animateToRegion(this.state.region, 1000)
            this._attemptReverseGeocodeAsync()
          })
        } catch (e) {
          this.setState({ error: e.message });
        } finally {
          this.setState({ inProgress: false });
        }
    };
    
    render(){
        
        const { params } = this.props.navigation.state;
        const campaign = params ? params.campaign : null
        const origin = params ? params.origin : null
        const currentData = params ? params.currentData : null
        const adSet = params ? params.adSet : null

        return(
            <View>
                <View>
                        {this.state.loaded ?
                            <View style={{marginTop: 0}}> 
                                <MapView
                                    ref={(map) => { this.map = map; }}
                                    style={{ alignSelf: 'stretch', height: '100%' }}
                                    initialRegion={this.state.region}
                                    onPress={(e) => this.onMapPress(e)}
                                >
                                {this.state.coordinate && <MapView.Marker
                                    coordinate={this.state.coordinate}
                                />}
                                {origin == 'audience_location' && <MapView.Circle
                                    onPress={() => {
                                        alert('pressed')
                                    }}
                                    center={this.state.coordinate}
                                    radius={this.state.radius}
                                    zIndex={3}
                                    strokeColor='rgb(66, 134, 244)'
                                    fillColor='rgba(66, 134, 244, .3)'
                                />}
                                </MapView>
                            </View>
                            : <ActivityIndicator style={{padding: '50%'}} size="large" color="#4a8bfc"/>
                        }
                        <View style={[genericStyles.section, genericStyles.mapSearchBar]}>
                            <View style={[formStyles.sectionGroup, {flexDirection: 'row'}]}>
                                <TextInput 
                                    placeholder='Search..'
                                    onChangeText={(address) => this.setState({address})}
                                    underlineColorAndroid='transparent'
                                    placeholderTextColor='#999'
                                    style={[formStyles.inputStyle, formStyles.withoutBorder ,{
                                        width: '87%',
                                        padding: width * 2.5 / 100,
                                    }]}>
                                </TextInput>
                                <Ionicons onPress={() => {
                                    this._attemptGeocodeAsync()
                                    Keyboard.dismiss()
                                }} style={{marginTop: 12}} name='ios-search' size={25} color='#999'/>
                            </View>
                        </View>
                        {origin == 'audience_location' ? <View style={genericStyles.mapCardContainer}>
                            <View style={genericStyles.inlineDivision}>
                                <Slider
                                    style={{
                                        width: '80%',
                                        alignSelf: 'center'
                                    }}
                                    minimumValue={500}
                                    maximumValue={50000}
                                    minimumTrackTintColor='#4286f4'
                                    maximumTrackTintColor='#99cfff'
                                    thumbTintColor='#4286f4'
                                    step={5}
                                    tipProps={{visible:true}}
                                    onValueChange={(radius) => {this.setState({radius: radius})}}
                                />
                                <Text style={
                                    {
                                        fontWeight: 'bold',
                                        color: '#999',
                                        
                                    }}>
                                    {Math.round((this.state.radius / 1000)) + ' km'}
                                </Text>
                            </View>
                            <GenericButton
                                style={[buttonsStyles.addButton, {
                                    borderRadius: 25,
                                    width: '90%',
                                    height: 40,
                                    marginTop: 15,
                                    alignSelf: 'center'
                                }]}
                                pressHandler={() => {
                                    this.props.navigation.navigate('CreateAudience', {
                                        location: {
                                            coords: this.state.coordinate, 
                                            address: this.state.currentAddress,
                                            radius: this.state.radius,
                                        },
                                        currentData: currentData,
                                        campaign: campaign
                                    })
                                }}
                                innerStyle={buttonsStyles.addButtonInnerStyle}
                                textStyle={buttonsStyles.addButtonText}
                                title='Set Location'
                                hasIcon={false}
                                loaded={true}
                            />
                        </View> : 
                            <GenericButton
                                    style={[buttonsStyles.addButton, {
                                    borderRadius: 25,
                                    width: '90%',
                                    position: 'absolute',
                                    bottom: 30,
                                    height: 40,
                                    marginTop: 15,
                                    alignSelf: 'center'
                                }]}
                                    pressHandler={() => {
                                        this.props.navigation.navigate('TwiganoAds', {
                                            location: {
                                                coords: this.state.coordinate, 
                                                address: this.state.currentAddress
                                            },
                                            campaign: campaign,
                                            adSet: adSet,
                                        })
                                    }}
                                    innerStyle={buttonsStyles.addButtonInnerStyle}
                                    textStyle={buttonsStyles.addButtonText}
                                    title='Set Location'
                                    hasIcon={false}
                                    loaded={true}
                            />}
                    </View>
            </View>
        )
    }
}