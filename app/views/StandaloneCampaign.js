import React, {Component} from 'react'
import { 
    View,
    ScrollView, 
    Text,  
    Alert,
    TextInput,
    StyleSheet,
    TouchableWithoutFeedback,
    ActivityIndicator,
    Switch } from 'react-native'
import PopupDialog, { DialogTitle, DialogButton } from 'react-native-popup-dialog'
import DriveTraffic from './../views/DriveTraffic'
import {formStyles} from './../configs/styles/form.styles'
import {genericStyles} from './../configs/styles/generic.styles'
import {buttonsStyles} from './../configs/styles/buttons.styles'
import GenericButton from './../components/GenericButton'
import GenericTabView from './../components/GenericTabView'
import { networkHandler } from './../configs/NetworkHandler' 
import { Ionicons } from '@expo/vector-icons'
import store from 'react-native-simple-store'
import Moment from 'moment'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import { getSinceTime } from '../configs/SinceTimeHandler';

export default class StandaloneCampaign extends Component {
    constructor(props){
        super(props)
        this.state = {
            status: false, 
            statusString: '',
            loaded: false,
            ad_set: null, 
            performanceData: [],
            results: [],
            creativeType: '',
            campaign_name: '',
            total_spent: 0,
            total_actions: 0,
            impressions: 0,
            budget: 0,
            cost_per_action: 0,
            duration:  ''
        }
        this.handleStatusChange = this.handleStatusChange.bind(this)
    }

    static navigationOptions = ({navigation}) => {
        
        const { params } = navigation.state;
        const name = params.campaign_name ? params.campaign_name : params.campaign.name 
        
        return {
            title: name,
            headerTitleStyle: {
                textAlign: 'center',
                alignSelf:'center'
            },
            headerLeft: (
                <Ionicons style={{marginLeft: 15, paddingHorizontal: 5}} onPress={() => {
                    navigation.navigate('Home')
                }} name='ios-arrow-back' size={28} color='#fff'/>
            ),
            headerRight: (<View></View>),
        }
    }
    

    componentWillMount(){
        
        const { params } = this.props.navigation.state;
        const campaign = params ? params.campaign : null 

        this.setState({
            statusString: campaign.status,
            status: campaign.status == 'running' ? true : false
        })

        this.fetchRecommendations(campaign)
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
        MessageBarManager.registerMessageBar(this.successMessage);
    }

    componentWillUnmount(){
        MessageBarManager.unregisterMessageBar();
    }

    handleStatusChange(){

        const { params } = this.props.navigation.state;
        const campaign = params ? params.campaign : null

        this.setState({
            status: !this.state.status
        },() => {
            var body = {
                _method: 'PUT',
                name: campaign.name,
                status: this.state.status ? 'running' : 'paused'
            }
            store.get('access_token').then((token) => {
                networkHandler.editCampaign(body, token, campaign.id, (data) => {
                    this.setState({
                        statusString: data.status
                    }, () => {
                        MessageBarManager.showAlert({
                            title: '',
                            message: this.state.status ? `${campaign.name} campaign is now running.`
                            : `You have paused ${campaign.name} campaign.`,
                            alertType: 'info',
                            duration: 4000,
                            position: 'bottom',
                        })
                    })
                })
            })
        })
    }

    fetchRecommendations(campaign){
        var totalActions = 0,
            impressions = 0
            budget = 0,
            costPerAction = 0;

        store.get('access_token').then((token) => 
            networkHandler.listCreatives(token, campaign.id ,(data) => {
                data.data.forEach(creative => {
                    totalActions += creative.clicks
                    impressions += creative.impressions
                }) 
            })     
        )

        store.get('access_token').then((token) => {
            networkHandler.fetchCampaign(token, campaign.id, (data) => {
                this.setState({
                    ad_set: data.ad_set,
                    total_spent: (totalActions * data.ad_set.billing_event_budget) ,
                    total_actions: totalActions,
                    impressions: impressions,
                    budget: data.ad_set.life_time_budget ? parseInt(data.ad_set.life_time_budget) : 0,
                    cost_per_action: data.ad_set.billing_event_budget ?
                     parseInt(data.ad_set.billing_event_budget)  : 0,
                    duration: getSinceTime(Moment(data.ad_set.end_time).unix(), Moment(data.ad_set.start_time).unix())
                }, () => {
                        performance = [
                            {name: 'Cost per Action', value: this.state.cost_per_action + ' EGP', key: 'cpa'},
                            {name: 'Total Actions', value: this.state.total_actions, key: 'actions'},
                            {name: 'Impressions', value: this.state.impressions, key: 'impressions'},
                            {name: 'Total Spent', value: this.state.total_spent + ' EGP', key: 'spent'},
                            {name: 'Duration', value: this.state.duration, key: 'duration'},
                            {name: 'Budget', value: this.state.budget + ' EGP', key: 'budget'},
                        ]
                        results= [
                            {total_spent: '55k', actions: 251},
                            {total_spent: '7k', actions: 35},
                            {total_spent: '1k', actions: 5},
                        ]
                        this.setState({
                            performanceData: performance,
                            results: results
                        }, () => {
                            this.setState({loaded: true})
                        }) 
                })
            })
        })
    }

    updateNameHandler(){
        
        const { params } = this.props.navigation.state;
        const campaign = params ? params.campaign : null

        if(this.state.campaign_name){
            var body = {
                _method: 'PUT',
                name: this.state.campaign_name,
                status: this.state.status ? 'running' : 'paused'
            }
            store.get('access_token').then((token) => {
                networkHandler.editCampaign(body, token, campaign.id, (data) => {
                    this.props.navigation.setParams({
                        campaign_name: data.name
                    });
                    this.popupDialog.dismiss()
                    this.setState({
                        campaign_name: ''
                    })
                })
            })
        }
        else {
            MessageBarManager.showAlert({
                title: '',
                message: 'Campaign name is required',
                alertType: 'error',
                duration: 4000,
                stylesheetError: {
                    backgroundColor: '#ff3232',
                    strokeColor: '#FF0000',
                    titleColor: '#ffffff',
                    messageColor: '#ffffff',
                    zIndex: 5000
                },
                position: 'bottom',
            })
        }
    }

    deleteHandler(id){
        store.get('access_token').then((token) => 
            networkHandler.deleteCampaign(token, id , (data) => {
               this.props.navigation.navigate('Home', {
                    refresh: true
               })
            })        
        )
    }

    showDeleteAlert(id){
        Alert.alert(
            'Delete Campaign',
            'Are you sure ?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => this.deleteHandler(id)},
            ],
            { cancelable: false }
        )
    }
    
    render(){

        const { params } = this.props.navigation.state;
        const campaign = params ? params.campaign : null

        return(
            <ScrollView contentContainerStyle={{flex: 0}}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <MessageBarAlert ref={(successMessage) => { this.successMessage = successMessage; }}/>
                <PopupDialog
                    containerStyle={{marginTop: -80,elevation: 4}}
                    width={.9}
                    dialogStyle={{backgroundColor: '#f5f5f5'}}
                    height={200}
                    dialogTitle={<DialogTitle title="Edit Campaign Name" />}
                    actions={[
                        <DialogButton
                            textContainerStyle={{paddingHorizontal: 10, paddingVertical: 8}}
                            textStyle={{color: '#fff', fontSize: 14}}
                            buttonStyle={[buttonsStyles.genericButton, {
                                backgroundColor: '#4d7bf3',
                                width: '80%',
                            }]}
                            text="Submit"
                            onPress={() => {
                                this.updateNameHandler()
                            }}
                            key="button-1"
                        />,
                    ]}
                    ref={(editCampaign) => { this.editCampaign = editCampaign; }}
                >
                    <View style={{flex: 1}}>
                        <TextInput 
                            placeholder='Campaign Name'
                            onChangeText={(campaign_name) => this.setState({campaign_name})}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            style={[formStyles.inputStyle, {width: '90%', backgroundColor: '#fff'}]}>
                        </TextInput>
                    </View>
                </PopupDialog>
                <PopupDialog
                    containerStyle={{marginTop: -80,elevation: 4}}
                    width={.9}
                    dialogStyle={{backgroundColor: '#f5f5f5'}}
                    height={350}
                    dialogTitle={<DialogTitle title="Select Creative Type" />}
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    actions={[
                        <DialogButton
                            text="Next"
                            textContainerStyle={{paddingHorizontal: 10, paddingVertical: 8}}
                            textStyle={{color: '#fff', fontSize: 14}}
                            buttonStyle={[buttonsStyles.genericButton, {
                                backgroundColor: '#4d7bf3',
                                width: '80%',
                            }]}
                            onPress={() => {
                                if(this.state.creativeType == 'ads'){
                                    this.props.navigation.navigate('TwiganoAds', {
                                        campaign: campaign,
                                        adSet: this.state.ad_set 
                                    })
                                } 
                                else {
                                    this.props.navigation.navigate('FeedProducts', {
                                        campaign: campaign,
                                        adSet: this.state.ad_set
                                    })
                                }
                            }}
                            key="button-1"
                        />,
                    ]}
                >
                    <View style={{flex: 1}}>
                        <DriveTraffic selectType={(type) => {
                            this.setState({creativeType: type})
                        }} isUpdate={true}/>
                    </View>
                </PopupDialog>
                {this.state.loaded ? <View>
                    <View style={genericStyles.section}>
                        <Text style={genericStyles.sectionHeader}>Info</Text>
                        <View style={[formStyles.inputGroup, formStyles.sectionGroup ,{flexDirection: 'row'}]}>
                            <Text style={[formStyles.inputStyle, formStyles.withoutBorder ,{width: '70%'}]}>
                            {`Campaign is ${this.state.statusString}`}
                            </Text>
                            <Switch
                                style={[formStyles.inputStyle, formStyles.withoutBorder ,{width: '20%'}]}
                                onValueChange={this.handleStatusChange}
                                value={this.state.status}
                            />
                        </View>
                        <View style={[formStyles.inputGroup, formStyles.sectionGroup ,{flexDirection: 'row'}]}>
                            <GenericButton
                                title={campaign.objective == 'traffic' ? 'Add Creative' : 'Edit Creative'}
                                loaded={true}
                                pressHandler={() => {
                                    if(campaign.objective == 'traffic') {
                                        this.popupDialog.show()
                                    }
                                    else {
                                        this.props.navigation.navigate('Awareness',{
                                            campaign: campaign
                                        })
                                    }
                                }}
                                style={styles.previewButton}
                                innerStyle={styles.previewButtonInnerStyle}
                                textStyle={styles.previewButtonText}
                            />
                            <GenericButton
                                title='Edit Name'
                                loaded={true}
                                pressHandler={() => {
                                    this.editCampaign.show()
                                }}
                                style={styles.previewButton}
                                innerStyle={styles.previewButtonInnerStyle}
                                textStyle={styles.previewButtonText}
                            />
                            <GenericButton
                                title='Delete'
                                pressHandler={() => {
                                    this.showDeleteAlert(campaign.id)
                                }}
                                loaded={true}
                                style={[styles.previewButton, {backgroundColor: '#dc3d4c'}]}
                                innerStyle={styles.previewButtonInnerStyle}
                                textStyle={styles.previewButtonText}
                            />
                        </View>
                    </View>
                    {/* <View style={genericStyles.section}>
                        <Text style={genericStyles.sectionHeader}>Results</Text>
                        <GenericTabView
                            style={styles.tabsStyle}
                            tabs={
                                [
                                    {text: 'Lifetime'},
                                    {text: '7 Days'},
                                    {text: '1 Day'}
                                ]
                            }
                            pages={this.state.results.map(result => {
                                        return(
                                            <View>
                                                <View style={{
                                                        flexWrap: 'wrap',
                                                        flexDirection: 'column',
                                                        alignItems: 'center',
                                                        justifyContent: 'center'
                                                    }}>
                                                    <Text
                                                        style={{
                                                            fontSize: 28,
                                                            color: '#999'
                                                        }}
                                                    >
                                                        {result.actions}
                                                    </Text>
                                                    <Text>Total Actions</Text>
                                                </View>
                                                <Text style={[formStyles.inputStyle, {marginTop: 13}]}>
                                                    Total Spent {result.total_spent}
                                                </Text>
                                            </View>
                                        )
                            })}
                        />
                    </View> */}
                    <View style={genericStyles.section}>
                        <Text style={genericStyles.sectionHeader}>Performance</Text>
                        {this.state.performanceData.map((entry, index) =>
                            <View key={entry.key} style={[formStyles.inputGroup, formStyles.sectionGroup ,
                            {flexDirection: 'row'}]}>
                                <Text style={[formStyles.inputStyle, formStyles.withoutBorder ,{width: '65%'}]}>
                                    {entry.name}
                                </Text>
                                <Text
                                    style={[formStyles.inputStyle, formStyles.withoutBorder ,{
                                        width: '30%',
                                        textAlign: 'right'
                                    }]}
                                >
                                {entry.value}
                                </Text>
                            </View>
                        )}
                    </View>
                    <TouchableWithoutFeedback>
                        <View style={{paddingBottom: 100}}>
                        </View>
                    </TouchableWithoutFeedback>
                </View> : 
                   <ActivityIndicator style={{padding: '50%'}} size="large" color="#4a8bfc"/>
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    previewButton: {
        width: '28%',
        backgroundColor: '#4a8bfc',
        marginRight: 8,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 5,
    },
    previewButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    previewButtonText: {
        color: '#fff',
        fontSize: 14,
        margin: 5,
        textAlign: 'center'
    },
    tabsStyle: {
        height: 150,
        backgroundColor: '#fff'
    }
})