import React, { Component } from 'react' 
import { View, Text, Alert, TouchableOpacity, ActivityIndicator } from 'react-native'
import { formStyles } from './../configs/styles/form.styles'
import { networkHandler } from '../configs/NetworkHandler'
import store from 'react-native-simple-store'
import { Col, Row, Grid } from "react-native-easy-grid"
import { genericStyles } from '../configs/styles/generic.styles'
import { buttonsStyles } from '../configs/styles/buttons.styles';


export default class AudienceSets extends Component {
    constructor(props){
        super(props)
        this.state = {
            audience: [],
            loaded: false, 
            selectedSet: ''
        }
    }

    static navigationOptions = {
        title: 'Audience Sets',
        headerTitleStyle: {
            textAlign: 'center',
            alignSelf:'center',
        },
        tabBarVisible: false, 
        swipeEnabled: false, 
        headerRight: (<View></View>)
    }

    componentDidMount(){
        this.fetchAudience()
    }

    fetchAudience(){
        store.get('access_token').then((token) => 
            networkHandler.listAudience(token , (data) => {
               this.setState({
                   audience: data.data,
                   loaded: true
               })
            })        
        )
    }

    deleteHandler(id){
        store.get('access_token').then((token) => 
            networkHandler.deleteAudience(token, id , (data) => {
               this.fetchAudience()
            })        
        )
    }

    updateHandler(id){
        store.get('access_token').then((token) => 
            networkHandler.deleteAudience(token, id , (data) => {
               this.fetchAudience()
            })        
        )
    }

    showUpdateDialogue(id){
        this.setState({
            selectedSet: id
        }, () => {
            this.popupDialog.show()
        })
    }

    showDeleteAlert(id){
        Alert.alert(
            'Delete Set',
            'Are you sure ?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => this.deleteHandler(id)},
            ],
            { cancelable: false }
        )
    }


    render(){
        return(
            <View style={{height: '100%'}}>
                {this.state.audience ? 
                (this.state.loaded ?
                this.state.audience.map((set, index) => {
                    return (
                        <View key={index} style={[genericStyles.section, {marginBottom: 0}]}>
                            <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                                <Grid>
                                    <Col>
                                        <Text style={[genericStyles.cardInfoItem, {width: '80%', paddingLeft: 5}]}>
                                            {set.name}
                                        </Text>
                                    </Col>
                                    <Col>
                                        <View style={[formStyles.inputGroup, {flexDirection: 'row'}]}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.props.navigation.navigate('CreateAudience', {
                                                        audience_set: set,
                                                        isUpdate: true
                                                    })
                                                }}
                                            >
                                                <View style={[buttonsStyles.button, {backgroundColor: '#0078d7'}]}>
                                                    <Text style={{color: '#fff'}}>Edit</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.showDeleteAlert(set.id)
                                                }}
                                            >
                                                <View style={[buttonsStyles.button, {backgroundColor: '#d03027'}]}>
                                                    <Text style={{color: '#fff'}}>Delete</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </Col>
                                </Grid>
                            </View>
                        </View>
                    ) 
                }): <ActivityIndicator size="large" color="#4a8bfc" style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}/>): <Text>No Sets</Text>}
            </View>
        )
    }
}
