import React, { Component } from 'react' 
import { View, Text, TextInput, Switch, TouchableHighlight } from 'react-native'
import { Location } from 'expo'
import { SegmentedControls } from 'react-native-radio-buttons'
import { formStyles } from './../configs/styles/form.styles'
import GenericButton from './../components/GenericButton'
import { networkHandler } from '../configs/NetworkHandler'
import store from 'react-native-simple-store'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'


export default class CreateAudience extends Component {
    constructor(props){
        super(props)
        this.state = {
            gender: 'Male',
            age_group: '13-18',
            audience_name: 'Audience Name',
            currentAddress: '',
            status: true,
            latitude: null,
            longitude: null,
            radius: null,
        }
        this.handleStatusChange = this.handleStatusChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    static navigationOptions = ({navigation}) => {

        const { params } = navigation.state;
        const name = params.audience_name ? params.audience_name : 'Create New Audience'
        
        return {
            title: name,
            headerTitleStyle: {
                textAlign: 'center',
                alignSelf:'center',
            },
            tabBarVisible: false, 
            swipeEnabled: false,
        }
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
        const { params } = this.props.navigation.state
        const currentData = params ? params.currentData : null
        const isUpdate = params ? params.isUpdate : null

        if(!isUpdate){
            this.setState({
                gender: currentData ? currentData.gender : 'Male',
                age_group: currentData ? currentData.age_group : '13-18',
                audience_name: currentData ? currentData.audience_name : '',
                status: currentData ? currentData.status : true 
            })
        }
        else {
            const { params } = this.props.navigation.state
            const audience_set = params ? params.audience_set : null

            this.props.navigation.setParams({
                audience_name: 'Update ' + audience_set.name
            })

            this.setState({
                gender: audience_set ? (audience_set.attributes.gender == 'any' ? 'Both' : 
                this.capitalizeFirstLetter(audience_set.attributes.gender)) : 'Male',
                    age_group: audience_set ? audience_set.attributes.min_age + '-' +
                audience_set.attributes.max_age : '13-18',
                audience_name: audience_set ? audience_set.name : '',
                status: audience_set ? audience_set.is_default : true,
                latitude: audience_set ? audience_set.attributes.location.latitude : '',
                longitude: audience_set ? audience_set.attributes.location.longitude : '',
                radius: audience_set ? audience_set.attributes.location.radius : ''
            }, () => {
                this._attemptReverseGeocodeAsync()
            })
        }
    }

    componentWillUnmount(){
        MessageBarManager.unregisterMessageBar();
    }

    handleStatusChange(){
        this.setState({
            status: !this.state.status
        })
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    handleAgeGroup(group, type){
        return type == 'max' ? parseInt(group.split('-')[1]) : parseInt(group.split('-')[0]) 
    }

    lowerCaseFirstLetter(string) {
        if(string == 'Both'){
            return 'any';
        }
        else {
            return string.charAt(0).toLowerCase() + string.slice(1);
        }
    }


    _attemptReverseGeocodeAsync = async () => {
        try {
            let result = await Location.reverseGeocodeAsync({
                latitude: this.state.latitude, longitude: this.state.longitude
            })
            this.setState({
                currentAddress: result[0].name
            })
        } catch (error) {
            
        }
    } 

    handleSubmit(){

        const { params } = this.props.navigation.state
        const location = params ? params.location : null
        const origin = params ? params.origin : null

        var body = {
            name: this.state.audience_name,
            is_default: this.state.status.toString(),
            gender: this.lowerCaseFirstLetter(this.state.gender),
            min_age: this.handleAgeGroup(this.state.age_group, 'min'),
            max_age: this.handleAgeGroup(this.state.age_group, 'max'),
            longitude: location ? location.coords.longitude.toFixed(5) : null,
            latitude: location ? location.coords.latitude.toFixed(5) : null,
            radius: location ? parseInt(location.radius / 1000) : null
        }


        store.get('access_token').then((token) => 
            networkHandler.createAudience(body,token, (data) => {
                if(data.id){
                    this.props.navigation.navigate(origin == 'TwiganoAds' ? 'TwiganoAds' : 'FeedProducts', {
                        refresh: true
                    })
                }
                else {
                    MessageBarManager.showAlert({
                        title: '',
                        message: data.name ? data.name : (data.latitude && 'Location is required'),
                        alertType: 'error',
                        duration: 6000,
                        position: 'bottom',
                    })
                }
            })
        )

    }

    handleUpdate(){

        const { params } = this.props.navigation.state
        const audience_set = params ? params.audience_set : null

        var body = {
            _method: 'PUT',
            name: this.state.audience_name,
            is_default: this.state.status.toString(),
            gender: this.lowerCaseFirstLetter(this.state.gender),
            min_age: this.handleAgeGroup(this.state.age_group, 'min'),
            max_age: this.handleAgeGroup(this.state.age_group, 'max'),
            longitude: this.state.longitude,
            latitude: this.state.latitude,
            radius: this.state.radius
        }


        store.get('access_token').then((token) => 
            networkHandler.updateAudience(body, audience_set.id ,token, (data) => {
                if(data.id){
                    this.props.navigation.goBack()
                }
                else {
                    MessageBarManager.showAlert({
                        title: '',
                        message: data.name ? data.name : (data.latitude && 'Location is required'),
                        alertType: 'error',
                        duration: 6000,
                        position: 'bottom',
                    })
                }
            })
        ) 
    }

    render(){
        
        const genderOptions = ['Male', 'Female', 'Both']
        const ageGroups = ['13-18', '19-30', '31-60']
        const { params } = !this.props.isUpdate && this.props.navigation.state
        const location = params ? params.location : null
        const isUpdate = params ? params.isUpdate : null

        return(
            <View style={[formStyles.formContainer, {backgroundColor: '#fff'}]}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <View style={[formStyles.inputGroup, {width: '90%'}]}>
                    <TextInput 
                        placeholder='Audience Name'
                        value={this.state.audience_name}
                        onChangeText={(audience_name) => this.setState({audience_name})}
                        underlineColorAndroid='transparent'
                        placeholderTextColor='#999'
                        style={formStyles.inputStyle}>
                    </TextInput> 
                </View>
                <View style={[formStyles.inputGroup, {width: '90%'}]}>
                    <Text style={formStyles.inputGroupLabel}>
                        Audience Gender
                    </Text>
                    <SegmentedControls
                        options={ genderOptions }
                        optionContainerStyle={{paddingTop: 8, paddingBottom: 8}}
                        onSelection={(selectedOption) => this.setState({gender: selectedOption})}
                        selectedOption={ this.state.gender }
                    />
                </View>
                <View style={[formStyles.inputGroup, {width: '90%'}]}>
                    <Text style={formStyles.inputGroupLabel}>
                        Audience Age Group
                    </Text>
                    <SegmentedControls
                        options={ ageGroups }
                        optionContainerStyle={{paddingTop: 8, paddingBottom: 8}}
                        onSelection={(selectedOption) => this.setState({age_group: selectedOption})}
                        selectedOption={ this.state.age_group }
                    />
                </View>
                <View style={[formStyles.inputGroup, {width: '90%'}]}>
                    <TouchableHighlight 
                        underlayColor='transparent'
                        onPress={() => {
                            this.props.navigation.navigate('SetLocationView', {
                                origin: 'audience_location',
                                currentData: {
                                    gender: this.state.gender,
                                    age_group: this.state.age_group,
                                    audience_name: this.state.audience_name,
                                    status: this.state.status
                                }
                            })
                        }}
                        style={[formStyles.inputStyle, {backgroundColor: '#ededed'}]}>
                        <Text>
                            {isUpdate ? this.state.currentAddress : 
                                (location ? location.address : 'Your Location')}
                        </Text>
                    </TouchableHighlight>
                </View>
                <View style={[formStyles.inputGroup, formStyles.sectionGroup ,{flexDirection: 'row'}]}>
                    <Text style={[formStyles.inputStyle, formStyles.withoutBorder ,{width: '70%'}]}>
                        Default Audience
                    </Text>
                    <Switch
                        style={[formStyles.inputStyle, formStyles.withoutBorder ,{width: '20%'}]}
                        onValueChange={this.handleStatusChange}
                        value={this.state.status}
                    />
                </View>
                <GenericButton
                    pressHandler={() => {
                        isUpdate ? this.handleSubmit() : this.handleUpdate() 
                    }}
                    style={[formStyles.actionButton, isUpdate && {height: '8.5%'}]} 
                    innerStyle={formStyles.actionButtonInnerStyle}
                    textStyle={formStyles.actionButtonText}
                    underlayColor="#008263"
                    loaded={true}
                    title={isUpdate ? 'Update' : 'Create'}
                    hasIcon={!isUpdate}
                    icon={{iconName: 'md-add', size: 22, color: '#fff'}}
                />
            </View>
        )
    }
}
