import React, { Component } from 'react' 
import { View, BackHandler, Alert } from 'react-native'
import StepsIndicator from './../components/GenericFormContainer'
import FeedProductsContentForm from './../components/FeedProductsContentForm'
import FeedProductsInfoForm from './../components/FeedProductsInfoForm'
import validate from '../configs/ValidationHandler'

export default class FeedProducts extends Component {
    constructor(props){
        super(props)
        this.state = {
            content: {},
            info: {},
        }
    }
    static navigationOptions = {
        tabBarVisible: false,
        swipeEnabled: false,
        header: null 
    }


    validateContent(state){
        var errorMessages = [
            validate('Products', this.state.content.products),
            validate('Name', state.name),
        ]
        this.contentForm.showErrorMessage(errorMessages)

    }

    shouldComponentUpdate(){
        return false
    }

    render(){

        const { params } = this.props.navigation.state;
        const campaign = params ? params.campaign : null
        const adSet = params ? params.adSet : null
        const refresh = params ? params.refresh : null
        const origin = params ? params.origin : null
        const { hasErrors } = this.props
        
        return(
            <View>
                <StepsIndicator 
                    steps={2}
                    formTitle='Feed Products'
                    labels={['Content', 'Info']}
                    getDatafromNavigate={() => {
                        var products = []
                        this.contentForm.state.inputsList.forEach(product => {
                            products.push(product.url)
                        })
                        this.setState({
                            content: {
                                products: products,
                                name: this.contentForm.state.name,
                                type: 'feed_products'
                            }
                        })
                        this.validateContent(this.contentForm.state)
                    }}
                    backButtonHandler={() => {
                        Alert.alert(
                            'Unsaved Changes',
                            'Unsaved Changes will be lost ?',
                            [
                                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                {text: 'OK', onPress: () => campaign.id ? 
                                    this.props.navigation.navigate('Campaign', {
                                        campaign: campaign
                                    }) 
                                    :
                                    this.props.navigation.navigate('Traffic')
                                },
                            ],
                            { cancelable: false }
                        )
                    }}
                    pages={[<FeedProductsContentForm
                        ref={(contentForm) => {this.contentForm = contentForm}}
                        sendData={(content) => {
                            this.setState({
                                content: content
                            })
                        }}
                        sendErrors={(errors) => {
                            this.contentForm.showErrorMessage(errors)
                        }}
                        disableSubmitButton={() => {
                            this.info.disableButton()
                        }}
                        enableSubmitButton={() => {
                            this.info.enableButton()
                        }}
                        />, 
                        <FeedProductsInfoForm 
                            adSet={adSet}
                            hasErrors={hasErrors}
                            ref={(info) => {this.info = info}}
                            refreshAudience={refresh}
                            navigateToPreview={(info) => {
                                this.setState({info: info}, () => {
                                    this.props.navigation.navigate('AdPreview', {
                                        content: this.state.content,
                                        info: this.state.info,
                                        campaign: campaign
                                    })
                                })
                            }}
                            navigateToAudience={() => {
                                this.props.navigation.navigate('CreateAudience', {
                                    origin: 'FeedProducts'
                                })
                            }}    
                        />
                    ]}
                />
            </View>
        )
    }
}
