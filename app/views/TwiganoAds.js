import React, { Component } from 'react' 
import { View, BackHandler, Alert } from 'react-native'
import StepsIndicator from './../components/GenericFormContainer'
import TwiganoAdsContentForm from './../components/TwiganoAdsContentForm'
import FeedProductsInfoForm from './../components/FeedProductsInfoForm'
import PopupDialog, { DialogTitle } from 'react-native-popup-dialog'
import { MapView, Permissions, Location } from 'expo'
import { formStyles } from '../configs/styles/form.styles'
import { buttonsStyles } from '../configs/styles/buttons.styles'
import validate from '../configs/ValidationHandler'

export default class TwiganoAds extends Component {
    constructor(props){
        super(props)
        this.state = {
            content: {},
            info: {},
            currentPage: null,
            hasErrors: false
        }
    }

    static navigationOptions = {
        tabBarVisible: false,
        swipeEnabled: false,
        header: null 
    }

    validateContent(state){
        var errorMessages = [
            validate('Message', state.message),
            validate('Name', state.name),
            validate('Action URL', state.link),
            validate('Media', state.media)
        ]
        this.contentForm.showErrorMessage(errorMessages)

    }

    shouldComponentUpdate(){
        return false
    }

    render(){

        const { params } = this.props.navigation.state
        const location = params ? params.location : null
        const campaign = params ? params.campaign : null
        const adSet = params ? params.adSet : null
        const refresh = params ? params.refresh : null
        const origin = params ? params.origin : null

        return(
            <View>
                <StepsIndicator
                    steps={2}
                    currentPage={(page) => {
                        this.setState({
                            currentPage: page
                        })
                    }}
                    formTitle='Twigano Ads'
                    labels={['Content', 'Info']}
                    getDatafromNavigate={() => {
                        this.setState({
                            content: {
                                media: this.contentForm.state.media,
                                media_type: this.contentForm.state.media_type,
                                call_to_action: this.contentForm.state.action_type,
                                caption: this.contentForm.state.message,
                                name: this.contentForm.state.name,
                                type: 'media',
                                action_url: this.contentForm.state.link,
                            }
                        })
                        this.validateContent(this.contentForm.state)
                    }}
                    backButtonHandler={() => {
                        Alert.alert(
                            'Unsaved Changes',
                            'Unsaved Changes will be lost ?',
                            [
                                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                {text: 'OK', onPress: () => campaign.id ? 
                                    this.props.navigation.navigate('Campaign', {
                                        campaign: campaign
                                    }) 
                                    :
                                    this.props.navigation.navigate('Traffic')
                                },
                            ],
                            { cancelable: false }
                        )
                    }}               
                    pages={
                        [
                            <TwiganoAdsContentForm
                                ref={(contentForm) => {this.contentForm = contentForm}}
                                sendData={(content) => {
                                    this.setState({content: content})
                                }}
                                disableSubmitButton={() => {
                                    this.info.disableButton()
                                }}
                                enableSubmitButton={() => {
                                    this.info.enableButton()
                                }}
                                setMedia={(media) => {
                                    this.setState({media: media})
                                }}
                                sendErrors={(errors) => {
                                    this.contentForm.showErrorMessage(errors)
                                }}
                                currentLocation={location ? location.address : 'Your Location'}
                                navigateToMap={() => {
                                    this.props.navigation.navigate('SetLocationView', {
                                        campaign: campaign,
                                        adSet: adSet
                                    })
                                }}
                            />,
                            <FeedProductsInfoForm
                                ref={(info) => {this.info = info}}
                                adSet={adSet}
                                refreshAudience={refresh}
                                navigateToPreview={(info) => {
                                    this.setState({info: info}, () => {
                                        this.props.navigation.navigate('AdPreview', {
                                            content: this.state.content,
                                            info: this.state.info,
                                            coords: location ? location.coords : {},
                                            campaign: campaign
                                        })
                                    })
                                }}
                                navigateToAudience={() => {
                                    this.props.navigation.navigate('CreateAudience', {
                                        origin: 'TwiganoAds'
                                    })
                                }}
                            />
                        ]
                    }
                />
            </View>
        )
    }
}
