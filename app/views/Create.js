import React, {Component} from 'react'
import { StyleSheet, View , Text, Alert, TouchableHighlight } from 'react-native'
import { networkHandler } from './../configs/NetworkHandler' 
import {formStyles} from './../configs/styles/form.styles'
import store from 'react-native-simple-store'
import GenericButton from './../components/GenericButton'
import PopupDialog, { DialogTitle, DialogButton } from 'react-native-popup-dialog'
import { Ionicons, Octicons } from '@expo/vector-icons'
import { LinearGradient } from 'expo'
import { genericStyles } from '../configs/styles/generic.styles';


export default class Create extends Component {
    constructor(props){
        super(props)
        this.state = {
            campaign_name: ''
        }
    }
    static navigationOptions = {
        title: 'Create New Campaign',
        headerTitleStyle: {
            textAlign: 'center',
            alignSelf:'center'
        },
        headerLeft: (<View></View>),
        headerRight: (<View></View>)
    }
    
    renderInfoButton(objective){
        var message = objective == 'traffic' ? 'Drive more customers to your online store, website or even your physical store with two modules: Feed Products and Twigano Ads' 
        : ' Expose your business to Twigano community and grow awareness for your brand with Twigano Banners'
        return (
            <GenericButton
                disabled={false}
                pressHandler={() => {
                    Alert.alert('', message)
                }}
                hasIcon={true}
                loaded={true}
                underlayColor='transparent'
                style={genericStyles.infoButtonStyle}
                icon={{iconName: 'ios-information-circle', size: 26, color: '#fff'}}
            />
        )
    }

    render(){
        return(
            <View style={styles.typeSelectorsContainer}>
                <LinearGradient colors={['#16d9e3', '#30c7ec', '#46aef7']} style={styles.typeSelector}>
                    <TouchableHighlight style={styles.typeSelectorInner} 
                    underlayColor="transparent" onPress={() => {
                        this.props.navigation.navigate('Traffic')
                    }}>
                        <View style={[styles.typeSelectorContent, formStyles.formContainer]}>
                                <Ionicons name='ios-speedometer' size={60} color='#fff'/>
                                <Text style={styles.typeSelectorText}>
                                    Drive Traffic
                                </Text>
                            {this.renderInfoButton('traffic')}
                        </View>
                    </TouchableHighlight>
                </LinearGradient>
                <LinearGradient colors={['#f093fb', '#f5576c']} style={styles.typeSelector}>
                    <TouchableHighlight style={styles.typeSelectorInner} 
                     underlayColor="transparent" onPress={() => {
                        this.props.navigation.navigate('Awareness')
                    }}>
                        <View style={[styles.typeSelectorContent, formStyles.formContainer]}>
                            <Octicons name='megaphone' size={57} color='#fff'/>
                            <Text style={styles.typeSelectorText}>
                                Grow Awareness
                            </Text>
                            {this.renderInfoButton('awareness')}
                        </View>
                    </TouchableHighlight>
                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    typeSelectorsContainer: {
        height: '100%',
        paddingTop: 40,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    typeSelector: {
        height: '40%',
        width: '90%',
        borderRadius: 5,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        shadowColor: "#ddd",
        shadowOffset: {
            width: 5,
            height: 2,
        },
        shadowOpacity: 0.08,
        shadowRadius: 10.46,
        elevation: 3,
    },
    typeSelectorInner: {
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
        justifyContent: 'center',
        width: '100%',
        height: '100%'
    },
    typeSelectorContent: {
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
        justifyContent: 'center'
    },
    typeSelectorText: {
        textAlign: 'center',
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold',
        marginLeft: 10
    }
})