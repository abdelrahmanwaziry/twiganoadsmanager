import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { LinearGradient } from 'expo';
import { buttonsStyles } from '../configs/styles/buttons.styles'
import GenericButton from '../components/GenericButton'

export default class BusinessAccount extends React.Component {
  render() {
    return (
        <View
        // colors={['#f5efef','#feada6']}
        style={styles.container}>
            <Text style={styles.message}>
                It seems that you don't have a business account.
            </Text>
            <View style={styles.actionGroup}>
                <GenericButton
                    loaded={true}
                    pressHandler={() => {
                        this.props.navigation.navigate('App')
                    }}
                    style={[buttonsStyles.placeOrderButton, {backgroundColor: '#ddd'}]}
                    innerStyle={[buttonsStyles.actionButtonInnerStyle, buttonsStyles.placeOrderButtonInnerStyle, {padding: 8}]}
                    textStyle={[buttonsStyles.actionButtonText, {fontWeight: 'normal', color: '#999'}]}
                    title='Skip For Now'
                    underlayColor="#ccc"
                />
                <GenericButton
                    loaded={true}
                    pressHandler={() => {
                        this.props.navigation.navigate('BusinessAccountForm')
                    }}
                    style={[buttonsStyles.placeOrderButton, {backgroundColor: '#4d7bf3'}]}
                    innerStyle={[buttonsStyles.actionButtonInnerStyle, buttonsStyles.placeOrderButtonInnerStyle, {padding: 8}]}
                    textStyle={[buttonsStyles.actionButtonText, {fontWeight: 'normal'}]}
                    title='Create Business Account'
                    underlayColor="#2962f7"
                />
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 0, 
        height: '100%', 
        width: '100%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'column',
    },
    message: {
        fontWeight: 'bold',
        fontSize: 23,
        width: '90%',
        color: '#444',
        textAlign: 'center'
    },
    actionGroup: {
        width: '100%',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    }
})