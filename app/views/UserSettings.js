import React, {Component} from 'react'
import {NavigationActions} from 'react-navigation'
import {ScrollView, View, StyleSheet, Text, TextInput, TouchableOpacity, Alert, TouchableWithoutFeedback} from 'react-native'
import { Ionicons, Octicons } from '@expo/vector-icons'
import PopupDialog, { DialogTitle, DialogButton } from 'react-native-popup-dialog'
import { formStyles } from '../configs/styles/form.styles'
import { genericStyles } from '../configs/styles/generic.styles'
import { buttonsStyles } from '../configs/styles/buttons.styles'
import GenericButton from './../components/GenericButton'
import CountryPicker, {getAllCountries} from 'react-native-country-picker-modal'
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input-fullpage"
import store from 'react-native-simple-store'
import { DangerZone } from 'expo'
import { networkHandler } from '../configs/NetworkHandler'
import getSymbolFromCurrency from 'currency-symbol-map'
import { formatNumber } from '../configs/BigNumbersHandler'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'

const {Localization} = DangerZone


export default class UserSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
          region: {},
          coordinate: {},
          cca2: '',
          loaded: false,
          country: {},
          balance: 0,
          new_balance: 0,
          currency: '',
          addCardText: {},
          cardInfo: {},
          isCardValid: true,
          user: {},
          invalidAmount: false,
          pressSource: '',
          loaded: true,
        }
    }

    static navigationOptions = {
        title: 'Profile',
        headerTitleStyle: {
            textAlign: 'center',
            alignSelf:'center'
        },
    }
    

    componentWillMount(){
        this.setState({
            addCardText: {title: 'Add New Card', button: 'Submit'}
        })
        store.get('user').then(user => {
            this.setState({
                user: user,
                currency: user.currency_code
            })
        })
        store.get('cardInfo').then(cardInfo => {
            this.setState({
                cardInfo: cardInfo
            })
        })
        this.fetchProfile()
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
    }

    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }

    fetchProfile(){
        store.get('access_token').then((token) => {
            networkHandler.fetchProfile(token, (data) => {
                this.setState({
                    balance: Math.round(data.business_account.balance / 100)
                }, () => {
                    store.save('balance', this.state.balance)
                })
            })
        })
    }

    appendCardValues(form){
        this.setState({
            cardInfo: {
                number: form.values.number,
                expiry: form.values.expiry,
                cvc: form.values.cvc,
                name: form.values.name,
                type: form.values.type,
            },
            isCardValid: form.valid 
        })
    }

    showErrorMessage(message){
        Alert.alert('', message)
    }

    handleLogout(){
        store.get('access_token').then((token) => {
            store.get('device_id').then((device_id) => {
                networkHandler.logoutHandler({device_id: device_id}, token, (data) => {
                    store.delete('access_token')
                    store.delete('balance')
                    store.delete('user')
                    this.props.navigation.navigate('Auth')
                })
            })
        })
    }

    updateBalance(value){
        this.setState(prevState => ({
            balance: Number(prevState.balance) + Number(value / 100)
        }), () => {
            store.save('balance', this.state.balance)
        })
    }
    
    checkout(amount){
        this.setState({loaded: false})
        if(this.state.cardInfo){
            var expiryDate = this.state.cardInfo.expiry.split('/')
            var checkoutBody = {
                identifier: this.state.cardInfo.number.replace(/\s+/g, ''),
                sourceholder_name: this.state.cardInfo.name,
                expiry_month: expiryDate[0],
                expiry_year: expiryDate[1],
                cvn: this.state.cardInfo.cvc,
                amount: parseInt(amount) * 100
            }
            store.get('access_token').then((token) => {
                networkHandler.rechargeBalance(checkoutBody, token, (data) => {
                    if(data.data){
                        this.updateBalance(data.data.payment.amount)
                        this.setState({loaded: true})
                    }
                    else {
                        var errorMessage = data.payment.length > 1 ? 'Invalid Credit Card' : 'Payment Declined'
                        this.showErrorMessage(errorMessage);
                        this.setState({loaded: true})
                    }
                }) 
            })
        } 
    }

    
    render(){
        return(
            <ScrollView style={{flex: 1, height: '100%'}}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <PopupDialog
                    containerStyle={{
                        flex: 1, 
                        height: '100%', 
                        justifyContent: 'center',
                        marginTop: -30,
                        elevation: 4
                    }}
                    width={.9}
                    height={500}
                    dialogTitle={<DialogTitle title={this.state.addCardText.title} />}
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    actions={[
                    <DialogButton
                        textContainerStyle={{paddingHorizontal: 10, paddingVertical: 8}}
                        textStyle={{color: '#fff', fontSize: 14}}
                        buttonStyle={[buttonsStyles.genericButton, {
                            backgroundColor: '#4d7bf3',
                            width: '80%',
                        }]}
                        text={this.state.addCardText.button}
                        onPress={() => {
                            this.popupDialog.dismiss()
                            if(this.state.isCardValid){
                                store.save('cardInfo', this.state.cardInfo)
                                if(this.state.pressSource !== 'credit_dialog') {
                                    setTimeout(() => {
                                        this.balanceDialog.show()
                                    }, 200)
                                }
                            }
                            else {
                                this.showErrorMessage('Please fill all card info')
                            }
                        }}
                        key="button-1"
                    />,
                    ]}
                    >
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <CreditCardInput
                            requiresName={true}
                            ref={(cardInput) => { this.cardInput = cardInput; }}
                            allowScroll={true}
                            onChange={(form) => {
                                this.appendCardValues(form)
                            }}
                        />
                    </View>
                </PopupDialog>
                <PopupDialog
                    containerStyle={{marginTop: -60, elevation: 4}}
                    width={.9}
                    height={200}
                    dialogTitle={<DialogTitle title='Recharge Your Balance' />}
                    ref={(balanceDialog) => { this.balanceDialog = balanceDialog; }}
                    actions={[
                    <DialogButton
                        textContainerStyle={{paddingHorizontal: 10, paddingVertical: 8}}
                        textStyle={{color: '#fff', fontSize: 14}}
                        buttonStyle={[buttonsStyles.genericButton, {
                            backgroundColor: this.state.invalidAmount ? '#b2c8ff' : '#4d7bf3',
                            width: '80%',
                        }]}
                        disabled={this.state.invalidAmount}
                        text='Submit'
                        onPress={() => {
                            this.checkout(this.state.new_balance)
                            this.balanceDialog.dismiss()
                        }}
                        key="button-1"
                    />,
                    ]}
                    >
                    <View style={{flex: 1}}>
                        <TextInput 
                            placeholder='Balance'
                            onChangeText={(new_balance) => {
                                this.setState({new_balance})
                            }}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            keyboardType='numeric'
                            style={[formStyles.inputStyle, {width: '90%', backgroundColor: '#fff'}]}>
                        </TextInput>
                    </View>
                </PopupDialog> 
                <View style={[genericStyles.section, {
                    flexDirection: 'row', 
                    justifyContent: 'center',
                    paddingTop: 20,
                    paddingBottom: 20,
                }]}>
                    <View style={{width: '50%', justifyContent: 'center'}}>
                        <Text style={[genericStyles.sectionHeader]}>Balance</Text>
                        <Text style={genericStyles.userCurrentBalance}>
                            { getSymbolFromCurrency(this.state.currency) + formatNumber(this.state.balance)}
                        </Text>
                    </View>
                    <View style={{width: '50%', justifyContent: 'center'}}>
                        <GenericButton
                            loaded={this.state.loaded}
                            pressHandler={() => {
                                if(!this.state.cardInfo){
                                    this.setState({
                                        addCardText: {title: 'Complete Billing Info First', button: 'Next'}
                                    }, () => {
                                        this.popupDialog.show()
                                    })
                                }
                                else {
                                    this.balanceDialog.show()
                                }
                            }}
                            style={[buttonsStyles.genericButton, {backgroundColor: '#4d7bf3'}]}
                            innerStyle={[buttonsStyles.genericButtonInner]}
                            textStyle={[buttonsStyles.genericButtonInnerContent, {fontWeight: 'normal'}]}
                            title='Recharge Balance'
                            underlayColor="#2962f7"
                        />
                    </View>
                </View>
                <View style={genericStyles.section}>
                    <Text style={genericStyles.sectionHeader}>Billing Info</Text>
                    <TouchableOpacity
                        onLongPress={() => {
                            Alert.alert(
                                'Delete Credit Card',
                                'Are you sure ?',
                                [
                                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                    {text: 'OK', onPress: () => {
                                        this.setState({cardInfo: null}, () => {
                                            store.delete('cardInfo')
                                        })
                                    }},
                                ],
                                { cancelable: false }
                            )
                        }}
                        onPress={() => {
                            this.popupDialog.show()
                            this.setState({
                                pressSource: 'credit_dialog'
                            })
                        }}
                    >
                    {!this.state.cardInfo ? 
                        <Text style={{padding: 12, fontSize: 15}}>
                            Add new card
                        </Text>
                        :
                        <View style={{marginLeft: 15}}>
                            <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                                <Ionicons
                                    name='ios-card' 
                                    size={28} 
                                    color='#999'
                                />
                                <Text style={styles.cardInfoItem}>
                                    {this.state.cardInfo.number}
                                </Text>
                            </View>
                            <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                                <Ionicons
                                    name='ios-person' 
                                    size={28} 
                                    color='#999'
                                />
                                <Text style={styles.cardInfoItem}>
                                    {this.state.cardInfo.name}
                                </Text>
                            </View>
                            <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                                <Ionicons
                                    name='ios-calendar' 
                                    size={28} 
                                    color='#999'
                                />
                                <Text style={styles.cardInfoItem}>
                                    {this.state.cardInfo.expiry}
                                </Text>
                            </View>
                        </View>
                    }
                    
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('AddContactInfo')
                    }}
                    >
                    <View style={genericStyles.section}>
                        <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                            <Text style={[styles.cardInfoItem, {width: '90%', paddingLeft: 5}]}>
                                Update Account Info
                            </Text>
                            <Ionicons
                                name='ios-person' 
                                size={32} 
                                color='#999'/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('UpdatePassword')
                    }}
                    >
                    <View style={genericStyles.section}>
                        <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                            <Text style={[styles.cardInfoItem, {width: '90%', paddingLeft: 5}]}>
                                Change Your Password
                            </Text>
                            <Ionicons
                                name='ios-lock' 
                                size={32} 
                                color='#999'/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('AudienceSets')
                    }}
                    >
                    <View style={genericStyles.section}>
                        <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                            <Text style={[styles.cardInfoItem, {width: '90%', paddingLeft: 5}]}>
                                View Audience Sets
                            </Text>
                            <Ionicons
                                name='ios-people' 
                                size={32} 
                                color='#999'/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.handleLogout()
                    }}
                    >
                    <View style={genericStyles.section}>
                        <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                            <Text style={[styles.cardInfoItem, {width: '90%', paddingLeft: 5}]}>
                                Logout
                            </Text>
                            <Ionicons
                                name='ios-log-out' 
                                size={32} 
                                color='#ed5e68'/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableWithoutFeedback>
                    <View style={{paddingBottom: !this.state.cardInfo ? 100 : 0}}>
                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10
    },
    instructions: {
      fontSize: 12,
      textAlign: 'center',
      color: '#888',
      marginBottom: 5
    },
    cardInfoItem: {
        width: '80%', 
        paddingLeft: 15,
        marginTop: 5,
        fontSize: 15,
        color: '#666',
        fontWeight: 'bold'
    }
  })