import React, {Component} from 'react'
import {RefreshControl, View, StyleSheet, Text, ActivityIndicator, ScrollView} from 'react-native'
import HomePreviewCard from './../components/HomePreviewCard'
import EmptyState from './../components/EmptyState'
import { networkHandler } from './../configs/NetworkHandler' 
import store from 'react-native-simple-store'
import SettingsButton from './../components/GenericButton'
import { formatNumber } from '../configs/BigNumbersHandler';
import GenericCard from '../components/GenericCard';

export default class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            banners: [],
            loaded: false,
            refreshing: false,
        }
    }
    
    
    static navigationOptions = ({navigation}) => {
        const { state, setParams, navigate } = navigation;
        const params = state.params || {};
        return {
            title: 'Home',
            headerTitleStyle: {
                textAlign: 'center',
                alignSelf: 'center',
            },
            headerLeft: (<View>
                <Text style={styles.headerBalance}>{ '$' + formatNumber(params.balance)}</Text>
            </View>),
            headerRight: (
                <View>
                </View>
            ),
        }
    }

    componentWillReceiveProps(){
        this.fetchProfile('lifecycle')
    }

    componentWillMount(){
        
        const { params } = this.props.navigation.state;
        const refresh = params ? params.refresh : null 

        if(refresh){
            this.fetchCampaigns('lifecycle') 
            this.fetchBanners('lifecycle')
            this.fetchProfile('lifecycle')
        }
        
        this.fetchProfile('lifecycle')
        this.fetchCampaigns('lifecycle')
        this.fetchBanners('lifecycle')

        store.get('balance').then(balance => {
            this.props.navigation.setParams({
                balance: balance
            });
        })
    }

    _onRefresh() {
        this.setState({refreshing: true});
        this.fetchCampaigns('refresh')
        this.fetchProfile('refresh')
        this.fetchBanners('refresh')
    }

    fetchProfile(source){
        store.get('access_token').then((token) => {
            networkHandler.fetchProfile(token, (data) => {
                this.props.navigation.setParams({
                    balance: Math.round(data.business_account.balance / 100)
                }, () => {
                    store.save('balance', this.state.balance)
                })
                if(source == 'refresh'){
                    this.setState({refreshing: false});
                }
            })
        })
    }

    fetchCampaigns(source){
        store.get('access_token').then((token) => 
            networkHandler.listCampaigns(token , (data) => {
               this.setState({
                   data: data.data,
                   loaded: true
               })
               if(source == 'refresh'){
                    this.setState({refreshing: false});
               }
            })        
        )
    }

    fetchBanners(source){
        store.get('access_token').then((token) => 
            networkHandler.listBanners(token , (data) => {
               this.setState({
                   banners: data.data,
                   loaded: true
               })
               if(source == 'refresh'){
                    this.setState({refreshing: false});
               }
            })        
        )
    }
  

    componentWillReceiveProps(){
        this.fetchCampaigns('lifecycle')
    }


    render(){
        return(
            <View style={styles.campaignsContainer}>
                {this.state.loaded ? 
                    (this.state.data.length > 0 ? <ScrollView 
                        refreshControl={<RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh.bind(this)}
                    />}
                    >
                        <View style={styles.campaignsHeader}>
                            <Text style={styles.campaignsHeaderText}>Campaigns</Text>
                        </View>
                        {this.state.data.map((campaign, index) => {
                            return(
                                <HomePreviewCard key={index} content={campaign} navigate={() => {
                                    this.props.navigation.navigate('Campaign', {
                                        campaign: campaign
                                    })
                                    }
                                }/>
                            )
                        })}
                        <View style={styles.campaignsHeader}>
                            <Text style={styles.campaignsHeaderText}>Banners</Text>
                        </View>
                        {this.state.banners.map((banner, index) => {
                            return(
                                <GenericCard key={index} data={banner}/>
                            )
                        })}
                    </ScrollView> : <EmptyState/>): 
                    <ActivityIndicator style={{padding: '50%'}} size="large" color="#4a8bfc"/>}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    campaignsContainer: {
        marginTop: 10,
        flex: 1,
    },
    campaignsHeader: {
        marginLeft: 15,
    },
    campaignsHeaderText: {
        fontWeight: '400',
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 10,
        color: '#999999',
        fontFamily: 'Roboto'
    },
    headerBalance: {
        fontSize: 20,
        fontWeight: '500',
        color: '#fff',
        marginLeft: 16
    }
})