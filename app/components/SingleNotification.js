import React, {Component} from 'react'
import {
        View,
        TouchableHighlight,
        Text,
        Image
} from 'react-native'
import store from 'react-native-simple-store'
import { genericStyles } from '../configs/styles/generic.styles'

export default class SingleNotification extends Component {    
    constructor(props){
        super(props)
        this.state = {
            authUser: {}
        }
    }

    componentDidMount(){
        store.get('user').then((user) => {
            this.setState({
                authUser: user
            })
        })
    }

    
    render(){
        const objectName = this.state.authUser.id == this.props.notification.object.id ? 'Your ' 
        : this.props.notification.object.name + ' '
        return(
            <TouchableHighlight 
                onPress={() => {
                    
                }}
                style={[genericStyles.cardStyle, !this.props.notification.read && genericStyles.unreadNotification]}
                underlayColor="#ffffff8a"
            >   
                <View style={genericStyles.notificationContainer}>
                    <View style={{margin: 10}}>
                        <Image
                            style={genericStyles.notificationThumb} 
                            source={{uri: this.props.notification.subject.thumbnail}}
                        />
                    </View>
                    <View style={genericStyles.notificationTextContainer}>
                        <Text style={genericStyles.notificationTitle}>
                            {objectName + this.props.notification.action}
                        </Text>
                        <Text style={genericStyles.notificationMessage}>
                            {this.props.notification.subject.message}
                        </Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}
