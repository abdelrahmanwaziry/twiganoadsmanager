import React, { Component } from 'react' 
import { View, 
        StyleSheet, 
        TouchableHighlight, 
        TextInput, 
        Text, 
        TouchableOpacity,
        Platform, 
        PickerIOS, 
        Alert,
        Dimensions,
        DatePickerIOS,
        DatePickerAndroid } from 'react-native'
import { Ionicons, Octicons } from '@expo/vector-icons'
import DateTimePicker from 'react-native-modal-datetime-picker'
import GenericButton from './GenericButton'
import { Picker } from 'react-native-picker-dropdown'
import { networkHandler } from '../configs/NetworkHandler'
import store from 'react-native-simple-store'
import Moment from 'moment'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import validate from '../configs/ValidationHandler';


export default class FeedProductsInfoForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            targted_audience: 1,
            selected_audience: {},
            budget_per_day: 0,
            budget: 0,
            audienceList: [],
            cost_per_click: 0,
            isStartDateTimePickerVisible: false,
            isEndDateTimePickerVisible: false,
            fromDate: null,
            toDate: null,
            fromText: 'Start Date',
            toText: 'End Date',
            hasErrors: false,
        }
        this.showStartDateTimePicker = this.showStartDateTimePicker.bind(this)
        this.hideStartDateTimePicker = this.hideStartDateTimePicker.bind(this)
        this.handleStartDatePicked = this.handleStartDatePicked.bind(this)
        this.showEndDateTimePicker = this.showEndDateTimePicker.bind(this)
        this.hideEndDateTimePicker = this.hideEndDateTimePicker.bind(this)
        this.handleEndDatePicked = this.handleEndDatePicked.bind(this)
    }

    componentWillMount(){
        this.fetchAudience()
        if(this.props.adSet){
            this.setState({
                budget: Math.round(this.props.adSet.life_time_budget),
                budget_per_day: Math.round(this.props.adSet.daily_budget),
                fromText: this.props.adSet.start_time ? this.props.adSet.start_time : 'Start Date',
                toText: this.props.adSet.end_time ? this.props.adSet.end_time : 'End Date',
                fromDate: this.props.adSet.start_time,
                toDate: this.props.adSet.end_time, 
                cost_per_click: this.props.adSet.billing_event_budget
            })
        }
    }

    componentDidMount(){
        if(this.props.refreshAudience){
            this.fetchAudience()
        }
        MessageBarManager.registerMessageBar(this.errorMessage);
    }

    fetchAudience(){
        store.get('access_token').then((token) => 
            networkHandler.listAudience(token, (data) => {
                var audience = data.data
                audience.push({ id: "0", name: 'Create New Audience +' })
                this.setState({
                    audienceList: audience,
                    selected_audience: data.data[0]
                })
            })
        )
    }

    getAudience(id){
        this.state.audienceList.forEach(set => {
            if(set.id == id){
                this.setState({
                    selected_audience: set
                })
            }
        });
    }

    showStartDateTimePicker = () => this.setState({ isStartDateTimePickerVisible: true });

    hideStartDateTimePicker = () => this.setState({ isStartDateTimePickerVisible: false });

    handleStartDatePicked = (date) => {
        this.setState({
            fromDate: Moment(date).format('YYYY-MM-DD HH:mm:ss')
        })
        this.hideStartDateTimePicker();
    }

    showEndDateTimePicker = () => this.setState({ isEndDateTimePickerVisible: true });

    hideEndDateTimePicker = () => this.setState({ isEndDateTimePickerVisible: false });

    handleEndDatePicked = (date) => {
        this.setState({
            toDate: Moment(date).format('YYYY-MM-DD HH:mm:ss')
        })
        this.hideEndDateTimePicker();
    }

    validateBudget(){
        if(Number(this.state.budget_per_day) > Number(this.state.budget)) {
            MessageBarManager.showAlert({
                title: '',
                viewTopOffset: 5,
                message: "Daily budget can't be bigger than lifetime budget.",
                alertType: 'error',
                position: 'bottom',
            })
            this.setState({
                hasErrors: true
            })
        }
        else if (Number(this.state.cost_per_click) > Number(this.state.budget_per_day) 
            || Number(this.state.cost_per_click) > Number(this.state.budget)) {
                MessageBarManager.showAlert({
                    title: '',
                    viewTopOffset: 5,
                    message: "Cost per action can't be bigger than lifetime budget or daily budget.",
                    alertType: 'error',
                    position: 'bottom',
                }) 
                this.setState({
                    hasErrors: true
                })
        }
        else {
            this.setState({
                hasErrors: false
            })
        }
    }

    enableButton(){
        this.setState({
            hasErrors: false
        })
    }

    disableButton(){
        this.setState({
            hasErrors: true
        })
    }

    showErrorMessage(errors){
        var messages = []
        errors.map((error, index) => {
            if(error !== null){
                messages.push(index !== errors.length - 1 ? `${error}, ` : `${error}.`)
            }
        })
        if(messages.length > 0){
            MessageBarManager.showAlert({
                title: '',
                message: messages,
                alertType: 'error',
                duration: 6000,
                position: 'bottom',
                messageNumberOfLines: 3
            })
            this.disableButton()
        }
        else {
            this.enableButton()
            this.props.navigateToPreview({
                daily_budget: this.state.budget_per_day,
                life_time_budget: this.state.budget,
                billing_event_budget: this.state.cost_per_click,
                start_time: this.state.fromDate,
                end_time: this.state.toDate,
                selected_audience: this.state.selected_audience
            })
        }
    }

    handleSubmit(){
        var errorMessages = [
            validate('Budget', this.state.budget), 
            validate('Daily Budget', this.state.budget_per_day),
            validate('Cost Per Click', this.state.cost_per_click),
            validate('Start Time', this.state.fromDate),
            validate('End Time', this.state.toDate),
        ]
        this.showErrorMessage(errorMessages)
    }

    render(){
        return(
            <View style={styles.formContainer}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <View style={styles.inputGroup}>
                    <TouchableOpacity style={[styles.inputStyle, styles.datePickerStyle 
                                ,{width: '45%'}]} 
                        onPress={this.showStartDateTimePicker}>
                        <Text>
                            {this.state.fromDate ? this.state.fromDate
                            : this.state.fromText}
                        </Text>
                    </TouchableOpacity>
                    <DateTimePicker
                        isVisible={this.state.isStartDateTimePickerVisible}
                        onConfirm={this.handleStartDatePicked}
                        mode='datetime'
                        onCancel={this.hideStartDateTimePicker}
                    />
                    <TouchableOpacity style={[styles.inputStyle, styles.datePickerStyle 
                            ,{width: '45%'}]} onPress={this.showEndDateTimePicker}>
                            <Text>
                                {this.state.toDate ? this.state.toDate 
                                : this.state.toText}
                            </Text>
                        </TouchableOpacity>
                        <DateTimePicker
                            isVisible={this.state.isEndDateTimePickerVisible}
                            onConfirm={this.handleEndDatePicked}
                            mode='datetime'
                            onCancel={this.hideEndDateTimePicker}
                        /> 
                    
                </View>
                <Text style={styles.inputGroupLabel}>
                    Budget Info
                </Text>
                <View style={styles.inputGroup}>
                    <TextInput 
                        placeholder='Budget'
                        value={this.state.budget.toString() !== '0' ? this.state.budget.toString() : null}
                        onChangeText={(budget) => { 
                            this.setState({budget: budget})
                        }}
                        onEndEditing={() => {
                            this.validateBudget()
                        }}
                        underlineColorAndroid='transparent'
                        placeholderTextColor='#999'
                        keyboardType='numeric'
                        style={[styles.inputStyle, {width: '45%'}]}>
                    </TextInput> 
                    <TextInput 
                        placeholder='Budget Per Day'
                        value={this.state.budget_per_day.toString() !== '0' ? this.state.budget_per_day.toString() : null}
                        onChangeText={(budget_per_day) => { 
                            this.setState({budget_per_day: budget_per_day})
                        }}
                        onEndEditing={() => {
                            this.validateBudget()
                        }}
                        underlineColorAndroid='transparent'
                        placeholderTextColor='#999'
                        keyboardType='numeric'
                        style={[styles.inputStyle, {width: '45%'}]}>
                    </TextInput> 
                </View>
                <View style={styles.inputGroup}>
                    <TextInput 
                        placeholder='Cost Per Click'
                        value={this.state.cost_per_click}
                        onChangeText={(cost_per_click) => this.setState({cost_per_click: cost_per_click})}
                        onEndEditing={() => {
                            this.validateBudget()
                        }}
                        underlineColorAndroid='transparent'
                        placeholderTextColor='#999'
                        keyboardType='numeric'
                        style={[styles.inputStyle, {width: '92%'}]}>
                    </TextInput> 
                </View>
                <Text style={styles.inputGroupLabel}>
                    Targeted Audience
                </Text>
                <View style={[styles.inputGroup, {justifyContent: 'center'}]}>
                    <Picker
                        selectedValue={this.state.targted_audience}
                        style={[styles.inputStyle, styles.pickerInputStyle, {width: '92%'}]}
                        mode='dropdown'
                        onValueChange={(itemValue, itemIndex) => {
                            this.setState({targted_audience: itemValue}, () => {
                                this.getAudience(this.state.targted_audience)
                            })
                            if(itemValue == 0){
                                this.props.navigateToAudience()
                                this.setState({
                                    targted_audience: this.state.audienceList[0].id
                                })
                            }
                        }}>
                        {this.state.audienceList.map((item, index) => {
                            return(
                                <Picker.Item style={styles.pickerItemStyle} 
                                key={index} label={item.name} value={item.id} />
                            )
                        })}
                    </Picker>
                </View>
                <GenericButton
                    pressHandler={() => {
                        this.handleSubmit()
                    }}
                    loaded={true}
                    disabled={this.state.hasErrors}
                    style={[styles.actionButton, this.state.hasErrors && {
                        backgroundColor: '#01cf9f3f'
                    }]} 
                    innerStyle={styles.actionButtonInnerStyle}
                    textStyle={styles.actionButtonText}
                    underlayColor="#008263"
                    title='Preview'
                    hasIcon={true}
                    icon={{iconName: 'ios-eye', size: 26, color: '#fff'}}
                />
            </View>    
        )
    }
}

let width = Dimensions.get('window').width

const styles = StyleSheet.create({
    formContainer: {
        width: '100%',
        height: '100%',
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'column',
    },
    scrollableView: {
        height: '100%'
    },
    inputGroup: {
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'row',
    },
    inputGroupLabel: {
        marginTop: 10,
        width: '90%',
        alignSelf: 'center',
        fontSize: 15,
        fontWeight: 'bold',
        color: '#aaa'
    },
    datePickerStyle: {
        padding: 14.5, 
    },
    pickerItemStyle: {
        padding: 30
    },
    inputStyle: {
        padding: Platform.OS == 'ios' ?  width * 4.5 / 100 : width * 3.5 / 100,
        paddingLeft: 10,
        borderRadius: 2,
        alignSelf: 'center',
        width: '90%',
        marginRight: 5,
        marginTop: 10,
        borderColor: '#d8d8d8',
        borderWidth: .5
    },
    pickerInputStyle: {
        padding: Platform.OS == 'ios' ? 15 : 2,
        backgroundColor: '#f0f0f0'
    },
    addButton: {
        width: '28%',
        borderRadius: 2,
        marginTop: 10,
        height: 55,
        padding: 10,
        backgroundColor: '#4a8bfc'
    },
    addButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    addButtonText: {
        color: '#fff',
        fontSize: 13,
        marginRight: 5,
        fontWeight: 'bold',
    },
    actionButton: {
        width: '100%',
        backgroundColor: '#01cf9e',
        position: 'absolute',
        bottom: 0,
        zIndex: -1,
        height: '10%',
    },
    actionButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',

    },
    actionButtonText: {
        color: '#fff',
        fontSize: 16,
        marginRight: 5,
        fontWeight: 'bold',
    }
})

