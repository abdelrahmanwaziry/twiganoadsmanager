import React, {Component} from 'react'
import {StyleSheet,
        View,
        Text,
        Button,
        Alert,
        Keyboard,
        ActivityIndicator,
        TouchableWithoutFeedback,
        Image, 
        TouchableOpacity,
        TouchableHighlight, 
        TextInput}
     from 'react-native'
import Logo from './../../app/configs/assets/media/login-logo.png'
import { networkHandler } from './../configs/NetworkHandler' 
import store from 'react-native-simple-store'
import { Ionicons } from '@expo/vector-icons'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import { formStyles } from '../configs/styles/form.styles';

const styles = StyleSheet.create({
    cardStyle: {
        width: '100%',
        height: '100%',
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff'
    },
    image: {
        alignSelf: 'center',
        marginTop: 100,
        marginBottom: 50,
        width: 100,
        height: 100,
        resizeMode: 'contain'
    },
    cardInner : {
        padding: 10,
        paddingLeft: 20,
        marginTop: 20,
        fontSize: 15,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#f2f2f2',
        borderRadius: 5,
        color: '#777'
    },
    buttonStyle: {
        padding: 10,
        top: 80,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#d80746',
        borderRadius: 5
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold'
    }
})

export default class Login extends Component {
    
    static navigationOptions =  ({navigation}) => {
        return {
            headerTitle: '',
            headerStyle: {
                backgroundColor: '#ea1f4a',
                height: -52,
                paddingTop: -25
            },
            tabBarVisible: false, 
            swipeEnabled: false,
        }
    }

    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: '',
            isClicked: false,
            device_id: '',
            reg_id: '',
            showPassword: false,
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
        const { params } = this.props.navigation.state;
        const refresh = params ? params.refresh : null 
        if(refresh){
            this.forceUpdate() 
        }
        store.get('device_id').then(device_id => {
            if(device_id){
                this.setState({
                    reg_id: 'erJinQ9Fg_8:APA91bEyNe5pleJX8wzaSo82tLgwxePY-drYydXYJlV0Y_QEaQmXFGMZ-yU6jFW1kVI9zcd0XadYQphKiRbHT7gcbyVUSZooJXqyKPC7oeND264AqaHUW8fGpa7NOL1lFP5BSkABrX3z',
					device_id: device_id
                })
            }
            else {
                this.setState({
                    reg_id: 'erJinQ9Fg_8:APA91bEyNe5pleJX8wzaSo82tLgwxePY-drYydXYJlV0Y_QEaQmXFGMZ-yU6jFW1kVI9zcd0XadYQphKiRbHT7gcbyVUSZooJXqyKPC7oeND264AqaHUW8fGpa7NOL1lFP5BSkABrX3z',
					device_id: this.generateRandomString(16)
                })
            }
        })
    }

    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }

    generateRandomString(length){
        var result = '';
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }

    handleInputVisibility(){
        this.setState({showPassword: !this.state.showPassword})
    }

    handleSubmit(){
        this.setState({
            isClicked: true
        })

        var body = {
			email: this.state.email,
			password: this.state.password,
			client_id: '1',
			grant_type: 'password',
			client_secret: 'bXxKzPBRQdvCD16g3M3AMFj81Wl74dzGCAg1jPH2'
        };
        
        var sessionBody = {
            reg_id: this.state.reg_id,
            device_id: this.state.device_id
        }

        networkHandler.loginHandler(body, (data) => {
            if(data.user){
                store.save('access_token', data.access_token)
                store.save('user', data.user)
                networkHandler.createSession(sessionBody, data.access_token, data.user.id, (session) => {
                    if(data.user.business_account.id){
                        this.props.navigation.navigate('App')
                        store.save('balance', Math.round(data.user.business_account.balance) / 100)
                    }
                    else {
                        this.props.navigation.navigate('BusinessAccount')
                    }
                    this.setState({
                        isClicked: false
                    })
                })
            }
            else {
                MessageBarManager.showAlert({
                    title: '',
                    message: data.email ? data.email : data.password,
                    alertType: 'error',
                    position: 'bottom',
                });
                this.setState({
                    isClicked: false
                })
            }
        })    	
    }

    render(){
        return(
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.cardStyle}>
                    <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                    <Image style={styles.image} source={Logo} />
                    <TextInput 
                        placeholder='Your Email'
                        onChangeText={(email) => this.setState({email})}
                        underlineColorAndroid='transparent'
                        style={styles.cardInner}>
                    </TextInput>
                    <View style={[styles.cardInner, formStyles.inputGroup, 
                            {
                                flexDirection: 'row',
                                paddingLeft: 10
                            }
                        ]}>
                        <TextInput 
                            placeholder='Your password'
                            underlineColorAndroid='transparent'
                            secureTextEntry={!this.state.showPassword}
                            style={{width: '90%'}}
                            onChangeText={(password) => this.setState({password})}>
                        </TextInput>
                        <TouchableOpacity onPress={() => {
                            this.handleInputVisibility()    
                        }}>
                            <Ionicons
                                name={this.state.showPassword ? 'ios-eye-off' : 'ios-eye' }
                                size={28} 
                                color='#888'
                            />
                        </TouchableOpacity>
                    </View>
                    <TouchableHighlight 
                        underlayColor="#b5093c" 
                        style={styles.buttonStyle} 
                        onPress={this.handleSubmit}>
                        <View>
                            {!this.state.isClicked ?
                            <Text style={styles.buttonText}>
                                Login
                            </Text> :
                            <ActivityIndicator size="small" color="#f2f2f2"/>}
                        </View>
                    </TouchableHighlight>
                    <Text style={{
                        width: '80%',
                        justifyContent: 'center',
                        marginLeft: 20,
                        fontWeight: 'bold'
                    }}
                        onPress={() => {this.props.navigation.navigate('Register')}}
                    >Don't have an account ? Register</Text>
                </View>               
            </TouchableWithoutFeedback>
        )
    }
}