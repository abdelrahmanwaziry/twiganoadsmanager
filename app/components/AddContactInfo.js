import React, {Component} from 'react'
import {View, StyleSheet} from 'react-native'
import BusinessAccountForm from './../components/BusinessAccountForm'
import store from 'react-native-simple-store'
import { networkHandler } from '../configs/NetworkHandler';

export default class AddContactInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    static navigationOptions = {
        title: 'Edit Contact Info',
        headerTitleStyle: {
            textAlign: 'center',
            alignSelf:'center',
        },
        tabBarVisible: false, 
        swipeEnabled: false, 
        headerRight: (<View></View>)
    }

    componentWillMount(){
        this.fetchProfile()
    }

    fetchProfile(){
        store.get('access_token').then((token) => {
            networkHandler.fetchProfile(token, (data) => {
                this.setState({data: data.business_account})
            })
        })
    }
    
    render(){
        return(
            <View style={{flex: 1, height: '100%', backgroundColor: '#fff'}}>
                <BusinessAccountForm navigateToHome={() => {
                    this.props.navigation.goBack()
                }} data={this.state.data}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10
    },
    instructions: {
      fontSize: 12,
      textAlign: 'center',
      color: '#888',
      marginBottom: 5
    },
    cardInfoItem: {
        width: '80%', 
        paddingLeft: 15,
        marginTop: 5,
        fontSize: 15,
        color: '#666',
        fontWeight: 'bold'
    },
    datePickerStyle: {
        padding: 14.5, 
    }
  })