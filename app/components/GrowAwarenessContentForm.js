import React, { Component } from 'react'
import {View, 
    StyleSheet, 
    TouchableHighlight, 
    TextInput, 
    Text, 
    Image,
    Alert,
    KeyboardAvoidingView,
    Platform} from 'react-native'
import { Ionicons, Octicons } from '@expo/vector-icons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { ImagePicker, Video, Permissions } from 'expo'
import validate from '../configs/ValidationHandler'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures'

export default class GrowAwarenessContentForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            media: '',
            media_type: 'image',
            url: '',
            name: '',
        }
        this.pickImage = this.pickImage.bind(this)
        this.cameraPermission = this.cameraPermission.bind(this)
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
    }

    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }
    

    showErrorMessage(errors){
        var messages = []
        errors.map((error, index) => {
            if(error !== null){
                messages.push(index !== errors.length - 1 ? `${error}, ` : `${error}.`)
            }
        })
        if(messages.length > 0){
            MessageBarManager.showAlert({
                title: '',
                message: messages,
                alertType: 'error',
                duration: 6000,
                viewTopInset: 5,
                viewBottomInset: 5,
                position: 'bottom',
            })
            this.props.disableSubmitButton()
        }
        else {
            this.props.enableSubmitButton()
        }
    }

    onSwipeLeft(gestureState){
        var errorMessages = [
            validate('Name', this.state.name),
            validate('Banner URL', this.state.url),
            validate('Media', this.state.media)
        ]
        this.props.sendErrors(errorMessages)
        this.props.sendData({
            media: this.state.media,
            media_type: this.state.media_type,
            action_url: this.state.url,
            name: this.state.name
        })
    }

    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [3, 1],
          mediaTypes: 'All'
        });
        if (!result.cancelled) {
            this.setState({ 
              media: result.uri,
              media_type: result.type
            })
            this.props.sendMedia(result.uri)
        }
    };

    cameraPermission = async () => {
        if(Platform.OS == 'ios'){
            const { Location, Permissions } = Expo;
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status === 'granted') {
                this.pickImage()
            } else {
                Alert.alert('Permission Denied', 'You need to allow Camera Roll Permission to pick an Image')
            }
        }
        else {
            this.pickImage()
        }
    }

    render(){
        return(
            <GestureRecognizer onSwipeLeft={(state) => this.onSwipeLeft(state)}>
            <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                {this.state.media ? 
                <TouchableHighlight underlayColor='transparent' 
                    onPress={this.state.media_type == 'image' && this.cameraPermission}
                    onLongPress={this.state.media_type == 'video' && this.cameraPermission}
                    > 
                    {this.state.media_type == 'image' ?
                    <Image style={styles.mediaPlaceholder} source={{uri: this.state.media}}/>
                    : <Video style={styles.mediaPlaceholder} 
                        resizeMode="cover"
                        shouldPlay
                        useNativeControls
                        isLooping source={{uri: this.state.media}}/>
                    }
                </TouchableHighlight>
                : <TouchableHighlight style={[styles.mediaPlaceholder, 
                    {backgroundColor: '#d1d1d1'}]}
                    underlayColor='transparent'
                    onPress={this.cameraPermission}>
                    <Ionicons name='ios-images-outline' size={55} color='#777'/>
                </TouchableHighlight>}
                <KeyboardAwareScrollView enableOnAndroid={true} enableAutoAutomaticScroll={(Platform.OS === 'ios')}>
                    <View style={styles.inputGroup}>
                        <Text style={styles.inputGroupLabel}>
                            Banner Name
                        </Text>
                        <TextInput 
                            placeholder='Enter banner name here'
                            onChangeText={(name) => this.setState({name})}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            style={[styles.inputStyle]}>
                        </TextInput>
                    </View>
                    <View style={styles.inputGroup}>
                        <Text style={styles.inputGroupLabel}>
                            Banner URL
                        </Text>
                        <TextInput 
                            placeholder='Enter banner URL here'
                            onChangeText={(url) => this.setState({url})}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            style={[styles.inputStyle]}>
                        </TextInput>
                    </View>
                </KeyboardAwareScrollView>
            </GestureRecognizer>
        )
    }

}

const styles = StyleSheet.create({
    formContainer: {
        width: '100%',
        height: '100%',
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'column',
    },
    mediaPlaceholder: {
        width: '100%',
        height: 170,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    inputGroup: {
        flexWrap: 'wrap', 
        alignSelf: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 5
    },
    inputGroupLabel: {
        marginTop: 10,
        width: '90%',
        alignSelf: 'center',
        fontSize: 15,
        fontWeight: 'bold',
        color: '#aaa'
    },
    inputStyle: {
        padding: 10,
        paddingLeft: 10,
        borderRadius: 2,
        alignSelf: 'center',
        width: '90%',
        marginRight: 5,
        marginTop: 10,
        borderColor: '#d8d8d8',
        borderWidth: .5
    },
    pickerInputStyle: {
        padding: 2,
        backgroundColor: '#f0f0f0'
    },
})