import React, { Component } from 'react' 
import { View, 
        StyleSheet, 
        TextInput, 
        Text,
        ImageBackground,
        Alert,
        TouchableWithoutFeedback,
        Dimensions} from 'react-native'
import { Ionicons, Octicons } from '@expo/vector-icons'
import swiperStyles from './../configs/styles/swiper.styles'
import {buttonsStyles} from './../configs/styles/buttons.styles'
import {formStyles} from './../configs/styles/form.styles'
import GenericButton from './GenericButton'
import PlacementCarousel from 'react-native-snap-carousel'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import store from 'react-native-simple-store'
import Moment from 'moment'
import validate from '../configs/ValidationHandler'

const {width, height} = Dimensions.get('window')

export default class GrowAwarenessInfoForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            places: [],
            currency: '',
            budget: 0,
            currentPlace: 2,
            days: 0,
            loaded: true,
            hasErrors: false,
        }
        this.renderItem = this.renderItem.bind(this)
        this.bidsCountHandler = this.bidsCountHandler.bind(this)
        this.selectHandler = this.selectHandler.bind(this)
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
        store.get('bidsCount').then(value => {
            if(value){
                if(new Date() !== value.day){
                    this.setState({
                        bidsCount: value.count
                    })
                }
            }
            else{
                this.setState({
                    bidsCount: 0
                })
            }
        })
        store.get('user').then(data => {
            this.setState({
                currency: data.currency_code
            })
        })
    }

    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }


    setCover(cover){
        const places = [
            {cover: cover, position: '2L'},
            {cover: cover, position: '1L'},
            {cover: cover, position: 'Center'},
            {cover: cover, position: '1R'},
            {cover: cover, position: '2R'}
        ]
        this.setState({
            places: places,
        })
    }


    bidsCountHandler(){
        if(this.state.bidsCount < 5){
            this.setState(prevState => ({ 
                bidsCount: prevState.bidsCount + 1 
            }),() =>{
                StorageHandler.setKey('bidsCount', {
                    count: this.state.bidsCount,
                    day: new Date()
                })
            });
        }
        else {
            Alert.alert('', "You've reached today's bids limit")
        }
    }

    selectHandler(index){
        this.setState({
            currentPlace: index
        })
    }

    setDuration(){
        var now = new Date();
        this.setState({
            start_time: Moment(now).add(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
            end_time: Moment(now).add(this.state.weeks, 'weeks').format('YYYY-MM-DD HH:mm:ss')
        })
    }

    renderItem({item, index}) {
        return (
            <TouchableWithoutFeedback onPress={() => {this.selectHandler(index)}}>
                <ImageBackground style={swiperStyles.slideInnerContainer} resizeMode='cover' source={{uri: item.cover}}>
                    <Text style={swiperStyles.slideInnerContainerText}>
                        {item.position}
                    </Text>
                    {this.state.currentPlace == index && 
                        <Ionicons style={swiperStyles.checkMark} 
                        name='ios-checkmark-circle' size={25} 
                    color='#fff'/>}
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }

    enableButton(){
        this.setState({
            hasErrors: false
        })
    }

    disableButton(){
        this.setState({
            hasErrors: true
        })
    }

    showErrorMessage(errors){
        var messages = []
        errors.map((error, index) => {
            if(error !== null){
                messages.push(index !== errors.length - 1 ? `${error}, ` : `${error}.`)
            }
        })
        if(messages.length > 0){
            MessageBarManager.showAlert({
                title: '',
                message: messages,
                alertType: 'error',
                duration: 6000,
                position: 'bottom',
                messageNumberOfLines: 3
            })
            this.disableButton()
        }
        else {
            this.enableButton()
            this.props.sendData({
                duration: this.state.days,
                slot: this.state.currentPlace + 1,
                budget: this.state.budget
            })
        }
    }

    setLoadingState(state){
        this.setState({loaded: state})
    }

    handleSubmit(){
        var errorMessages = [
            validate('Bid', this.state.budget), 
            validate('Duration', this.state.days),
        ]
        this.showErrorMessage(errorMessages)
    }


    render(){
        return(
            <View>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <View style={formStyles.formContainer}>
                    <Text style={[formStyles.inputGroupLabel, {paddingLeft: 15}]}>
                        Banner Placement
                    </Text>
                    <PlacementCarousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.places}
                        onSnapToItem={(slideIndex) => {
                            this.setState({
                                currentPlace: slideIndex
                            })
                            this.forceUpdate()
                        }}
                        layout={'default'}
                        containerCustomStyle={{flexGrow: 0}}
                        contentContainerCustomStyle={{height: 150}}
                        renderItem={this.renderItem}
                        sliderWidth={360}
                        itemWidth={256}
                        layout={'default'}
                        firstItem={2}
                    />
                    {/* <View style={styles.inputGroup}>
                        <TextInput 
                            placeholder='Biding Value'
                            onChangeText={(value) => this.setState({value})}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            keyboardType='numeric'
                            style={styles.inputStyle}>
                        </TextInput>
                        <GenericButton
                            pressHandler={this.bidsCountHandler}
                            disabled={!this.state.value}
                            style={[buttonsStyles.addButton, !this.state.value && buttonsStyles.disabled]} 
                            innerStyle={buttonsStyles.addButtonInnerStyle}
                            textStyle={buttonsStyles.addButtonText}
                            underlayColor="#c61b40"
                            title='Submit Bid'
                            hasIcon={false}
                        />
                    </View>
                    <Text style={styles.counter}>{`${this.state.bidsCount} out of 5 bids`}</Text> */}
                    <View style={[formStyles.inputGroup, formStyles.inputStyle, {
                            flexDirection: 'row', width: '90%', padding: 0
                        }]}>
                            <TextInput 
                                placeholder='Banner Duration'
                                underlineColorAndroid='transparent'
                                placeholderTextColor='#999'
                                keyboardType='numeric'
                                onChangeText={(days) => {
                                    this.setState({days: days})
                                }}
                                style={{width: '70%', padding: 12}}>
                            </TextInput>
                            <View style={[styles.inputMask]}>
                                <Text style={styles.inputMaskText}>
                                    Days
                                </Text>
                            </View>
                    </View>
                    <View style={[formStyles.inputGroup, formStyles.inputStyle, {
                            flexDirection: 'row', width: '90%', padding: 0
                        }]}>
                            <TextInput 
                                placeholder='Budget'
                                underlineColorAndroid='transparent'
                                placeholderTextColor='#999'
                                keyboardType='numeric'
                                onChangeText={(budget) => this.setState({budget})}
                                style={{width: '70%', padding: 12}}>
                            </TextInput>
                            <View style={[styles.inputMask]}>
                                <Text style={styles.inputMaskText}>
                                    {this.state.currency}
                                </Text>
                            </View>
                    </View>
                    <GenericButton
                        pressHandler={() => {
                            this.handleSubmit()
                        }}
                        disabled={false}
                        style={[buttonsStyles.actionButton, {height: '10%'} , this.state.hasErrors && {
                            backgroundColor: '#01cf9f3f'
                        }]} 
                        innerStyle={buttonsStyles.actionButtonInnerStyle}
                        textStyle={buttonsStyles.actionButtonText}
                        underlayColor="#008263"
                        title='Place Order'
                        loaded={this.state.loaded}
                    />
                </View>
            </View>    
        )
    }
}


const styles = StyleSheet.create({
    formContainer: {
        width: '100%',
        height: '100%',
        flexWrap: 'wrap', 
        alignSelf: 'center',
    },
    scrollableView: {
        height: '100%'
    },
    counter: {
        marginTop: 10,
        marginBottom: 10,
        width: '90%',
        alignSelf: 'center',
        fontSize: 15,
        fontWeight: 'bold',
        color: '#aaa'
    },
    inputMask: {
        backgroundColor: '#eee',
        width: '30%',
        padding: 12
    },
    inputMaskText: {
        textAlign: 'center',
        color: '#999',
        fontSize: 16,
    }
})

