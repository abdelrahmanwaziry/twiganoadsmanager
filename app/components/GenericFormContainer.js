import React, { Component } from 'react' 
import { View, StyleSheet, Text } from 'react-native'
import GenericButton from './GenericButton'
import { ViewPager } from 'rn-viewpager';
import StepIndicator from 'react-native-step-indicator';

const indicatorStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: '#4a8bfc',
    stepStrokeWidth: 5,
    stepStrokeFinishedColor: '#4a8bfc',
    stepStrokeUnFinishedColor: '#dedede',
    separatorFinishedColor: '#4a8bfc',
    separatorUnFinishedColor: '#dedede',
    stepIndicatorFinishedColor: '#4a8bfc',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 0,
    currentStepIndicatorLabelFontSize: 0,
    stepIndicatorLabelCurrentColor: 'transparent',
    stepIndicatorLabelFinishedColor: 'transparent',
    stepIndicatorLabelUnFinishedColor: 'transparent',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#4a8bfc'
}

export default class GenericFormContainer extends Component {
    constructor(props){
        super(props)
        this.state = {
            currentStep: 0,
            currentPage: null
        }
    }


    componentWillReceiveProps(nextProps,nextState) {
        if(nextState.currentStep != this.state.currentStep) {
          if(this.viewPager) {
            this.viewPager.setPage(nextState.currentStep)
          }
        }
    }

    render(){
        return(
            <View>
                <View style={styles.stepsIndicatorContainer}>
                    <View style={styles.stepIndicatorHeader}>
                        <GenericButton
                            disabled={false}
                            pressHandler={() => {
                                if(this.state.currentStep === 0){
                                    this.props.backButtonHandler()
                                }
                                else {
                                    this.viewPager.setPage(0)
                                }
                            }}
                            hasIcon={true}
                            loaded={true}
                            underlayColor='transparent'
                            style={styles.backButtonStyle}
                            icon={{iconName: 'md-arrow-round-back', size: 26, color: '#999'}}
                        />
                        {this.state.currentStep === 0 &&
                            <GenericButton
                                disabled={false}
                                pressHandler={() => {
                                    this.viewPager.setPage(1)
                                    this.props.getDatafromNavigate()
                                }}
                                hasIcon={true}
                                loaded={true}
                                underlayColor='transparent'
                                style={styles.forwardButtonStyle}
                                icon={{iconName: 'md-arrow-round-forward', size: 26, color: '#999'}}
                            />
                        }
                        <Text style={styles.stepsText}>{this.props.formTitle}</Text>
                        <StepIndicator 
                            stepCount={this.props.steps} 
                            customStyles={indicatorStyles}
                            currentPosition={this.state.currentStep}
                            labels={this.props.labels}
                        />
                    </View>    
                    <ViewPager
                        style={{flex:1}}
                        ref={(viewPager) => {this.viewPager = viewPager}}
                        onPageSelected={(page) => {
                            this.setState({
                                currentStep: page.position
                            })
                        }}
                    >
                        {this.props.pages.map((page) => {
                            return(
                                this.renderViewPagerPage(page)
                            )
                        })}
                    </ViewPager>
                </View>
            </View>
        )
    }

    renderViewPagerPage = (page) => {
        return(
            <View>
                {page}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    stepsIndicatorContainer: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
    },
    stepIndicatorHeader: {
        backgroundColor: '#f2f2f2',
        paddingTop: 20,
        paddingBottom: 20
    },
    stepsText: {
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 5,
        fontWeight: 'bold',
        fontSize: 18,
        color: '#999'
    },
    backButtonStyle: {
        position: 'absolute',
        top: 5,
        left: 10,
        padding: 10
    },
    forwardButtonStyle: {
        position: 'absolute',
        top: 5,
        right: 10,
        padding: 10
    }
})