import React, {Component} from 'react'
import {View, Text} from 'react-native'
import { PagerTabIndicator, IndicatorViewPager } from 'rn-viewpager'
import {formStyles} from './../configs/styles/form.styles'


export default class GenericTabView extends Component {
    
    renderTabIndicator() {
        return <PagerTabIndicator
                    itemStyle={{paddingTop: 4}} 
                    selectedItemStyle={{paddingTop: 4}}
                    selectedTextStyle={{fontSize: 15}} 
                    textStyle={{fontSize: 15}} 
                    tabs={this.props.tabs} 
                    changePageWithAnimation={true}
                />
    }
    
    render(){
        return(
            <IndicatorViewPager style={this.props.style} indicator={this.renderTabIndicator()}>
                {this.props.pages}
            </IndicatorViewPager>
        )
    }
}