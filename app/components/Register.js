import React, {Component} from 'react'
import {StyleSheet,
        View,
        Text,
        ActivityIndicator,
        Image, 
        TouchableOpacity,
        TouchableHighlight, 
        TextInput}
     from 'react-native'
import Logo from './../../app/configs/assets/media/login-logo.png'
import { networkHandler } from '../configs/NetworkHandler'
import { Ionicons } from '@expo/vector-icons'
import store from 'react-native-simple-store'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import { formStyles } from '../configs/styles/form.styles'

const styles = StyleSheet.create({
    cardStyle: {
        width: '100%',
        height: '100%',
        flex: 0,
        backgroundColor: '#fff'
    },
    scrollableView: {
        height: '60%',
        marginBottom: 40,
    },
    image: {
        alignSelf: 'center',
        marginTop: 100,
        marginBottom: 50,
        width: 100,
        height: 100,
        resizeMode: 'contain'
    },
    cardInner : {
        padding: 10,
        paddingLeft: 20,
        marginTop: 20,
        fontSize: 15,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#f2f2f2',
        borderRadius: 5,
        color: '#777'
    },
    buttonStyle: {
        padding: 10,
        marginTop: 10,
        marginBottom: 10,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#d80746',
        borderRadius: 5
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold'
    }
})

export default class Register extends Component {

    constructor(props){
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            showPassword: false,
            isClicked: false,
            reg_id: '',
            device_id: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount(){
        store.get('device_id').then(device_id => {
            if(device_id){
                this.setState({
                    reg_id: 'erJinQ9Fg_8:APA91bEyNe5pleJX8wzaSo82tLgwxePY-drYydXYJlV0Y_QEaQmXFGMZ-yU6jFW1kVI9zcd0XadYQphKiRbHT7gcbyVUSZooJXqyKPC7oeND264AqaHUW8fGpa7NOL1lFP5BSkABrX3z',
					device_id: device_id
                })
            }
            else {
                this.setState({
                    reg_id: 'erJinQ9Fg_8:APA91bEyNe5pleJX8wzaSo82tLgwxePY-drYydXYJlV0Y_QEaQmXFGMZ-yU6jFW1kVI9zcd0XadYQphKiRbHT7gcbyVUSZooJXqyKPC7oeND264AqaHUW8fGpa7NOL1lFP5BSkABrX3z',
					device_id: this.generateRandomString(16)
                })
            }
        })
    }

    generateRandomString(length){
        var result = '';
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }
    

    handleInputVisibility(){
        this.setState({showPassword: !this.state.showPassword})
    }

    createSession(data){
        
        var sessionBody = {
            reg_id: this.state.reg_id,
            device_id: this.state.device_id
        }

        networkHandler.createSession(sessionBody, data.access_token, data.user.id, (data) => {
            this.props.navigation.navigate('BusinessAccount')
            this.setState({
                isClicked: false
            })
        })
    }
    
    handleSubmit(){
        this.setState({isClicked: true})
        
        var body = {
            name: this.state.name,
			email: this.state.email,
            password: this.state.password,
        }
        
        networkHandler.registerHandler(body, (data) => {
            if(data.user){
                store.save('access_token', data.access_token)
                store.save('user', data.user)
                store.save('balance', 0)
                this.createSession(data)
            }
            else {
                MessageBarManager.showAlert({
                    title: '',
                    message: data.email ? data.email : data.password,
                    alertType: 'error',
                    position: 'bottom',
                });
                this.setState({
                    isClicked: false
                })
            }
        })
		
    }

    render(){
        return(
            <View style={styles.cardStyle}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <Image style={styles.image} source={Logo}/>
                <TextInput 
                        placeholder='Name'
                        underlineColorAndroid='transparent'
                        onChangeText={(name) => this.setState({name})}
                        style={styles.cardInner}>
                </TextInput>
                <TextInput 
                    placeholder='Email'
                    keyboardType='email-address'
                    underlineColorAndroid='transparent'
                    onChangeText={(email) => this.setState({email})}
                    style={styles.cardInner}>
                </TextInput>
                <View style={[styles.cardInner, formStyles.inputGroup, 
                        {
                            flexDirection: 'row',
                            paddingLeft: 10
                        }
                    ]}>
                    <TextInput 
                        placeholder='Your password'
                        underlineColorAndroid='transparent'
                        secureTextEntry={!this.state.showPassword}
                        style={{width: '90%'}}
                        onChangeText={(password) => this.setState({password})}>
                    </TextInput>
                    <TouchableOpacity onPress={() => {
                        this.handleInputVisibility()    
                    }}>
                        <Ionicons
                            name={this.state.showPassword ? 'ios-eye-off' : 'ios-eye' }
                            size={28} 
                            color='#888'
                        />
                    </TouchableOpacity>
                </View>
                <Text style={{
                        width: '80%',
                        justifyContent: 'center',
                        marginLeft: 20,
                        margin: 15,
                        fontWeight: 'bold'
                    }}
                        onPress={() => {this.props.navigation.navigate('Login')}}
                    >Already have an account ? Login</Text>
                <TouchableHighlight 
                    underlayColor="#b5093c" 
                    style={styles.buttonStyle} 
                    onPress={this.handleSubmit}>
                    <View>
                        {!this.state.isClicked ?
                        <Text style={styles.buttonText}>
                            Register
                        </Text> :
                        <ActivityIndicator size="small" color="#f2f2f2"/>}
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}