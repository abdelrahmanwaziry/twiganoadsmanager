import React, {Component} from 'react'
import {
        StyleSheet,
        View,
        Image, 
        ToastAndroid,
        TouchableHighlight,
        PixelRatio,
        Text
} from 'react-native'
import { Ionicons, Octicons } from '@expo/vector-icons'
import Placeholder from './../../app/configs/assets/media/placeholder.png'

export default class HomePreviewCard extends Component {
    
    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    render(){
        return(
            <TouchableHighlight 
                onPress={this.props.navigate} 
                style={styles.cardStyle}
                underlayColor="#fff"
            >
                <View>
                    <View style={styles.cardInner}>
                        <View style={styles.campaignImage}>
                            {this.props.content.objective == 'traffic' ? 
                            <Ionicons name='ios-speedometer' size={40} color='#999'/>
                            : <Octicons name='megaphone' size={40} color='#999'/>}
                        </View>
                        <View style={styles.campaignInfo}>
                            <Text style={styles.campaignName}>
                                {this.props.content.name}
                            </Text>
                            <View style={styles.status}>
                                <View style={[styles.circle, this.props.content.status == 'running' && {
                                    backgroundColor: '#01cf9e'
                                }]}></View>
                                <Text style={styles.statusText}>
                                    {this.capitalizeFirstLetter(this.props.content.status)}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
    cardStyle: {
        width: '90%',
        alignSelf: 'center',
        borderRadius: 3,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: '#fff',
        shadowColor: "#222",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.08,
        shadowRadius: 2.46,
        elevation: 2,
    },
    cardInner : {
        padding: 15,
        flexWrap: 'wrap', 
        alignItems: 'flex-start',
        flexDirection:'row',
    },
    stats: {
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row',
        padding: 15,
    },
    campaignImage: {
        marginTop: 10,
        marginRight: 10,
        borderRadius: 100,
    },
    campaignInfo: {
        width: '70%'
    },
    campaignName: {
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 10,
        marginBottom: 2,
        fontFamily: 'Roboto',
        color: '#2c2c2c',
    },
    campaignType: {
        fontSize: 12,
        marginTop: 10,
        marginBottom: 2,
        fontFamily: 'Roboto',
        color: '#999',
    },
    status: {
        flexWrap: 'wrap',
        marginTop: 5, 
        marginBottom: 2,
        alignItems: 'flex-start',
        flexDirection:'row',
    },
    statusText: {
        marginTop: -6,
        fontFamily: 'Roboto',
        color: '#949494'
    },
    circle: {
        height: 10,
        width: 10,
        marginLeft: 2,
        marginRight: 5,
        backgroundColor: '#f70f14',
        borderRadius: 100,
    }
})