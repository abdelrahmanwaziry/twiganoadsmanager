import React, { Component } from 'react' 
import { 
    View, 
    Text, 
    TextInput, 
    StyleSheet, 
    ActivityIndicator,
    Alert, 
    Platform
} from 'react-native'
import AddProduct from './../components/GenericButton'
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures'
import { Ionicons, Octicons } from '@expo/vector-icons'
import validate from './../configs/ValidationHandler'
import { formStyles } from '../configs/styles/form.styles'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class FeedProductsContentForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            camapign_type: 'feed',
            inputsList: [],
            name: '',
            hasErrors: false
        }
        this.appendNewInput = this.appendNewInput.bind(this)
        this.handleURLChange = this.handleURLChange.bind(this)
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
    }

    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }

    componentWillMount(){
        var inputsArr = []
        for (let index = 0; index < 4; index++) {
            inputsArr.push(
                {
                    index: index,
                    url: '',
                    hasError: false,
                    active: false,
                    loading: false
                }
            )
        }
        this.setState({
            inputsList: inputsArr
        })
    }
    
    handleLoadingState(index, status){
        var inputs = [...this.state.inputsList];
        var id = inputs.findIndex(obj => obj.index === index);
        inputs[id].loading = status
        this.setState({
            inputsList: inputs
        })
    }

    handlehasErrorState(index, status){
        var inputs = [...this.state.inputsList];
        var id = inputs.findIndex(obj => obj.index === index);
        inputs[id].hasError = status
        this.setState({
            inputsList: inputs
        })
    }

    handleActiveState(index, status){
        var inputs = [...this.state.inputsList];
        var id = inputs.findIndex(obj => obj.index === index);
        inputs[id].active = status
        this.setState({
            inputsList: inputs
        })
    }
    
    searchHandler(index){
        this.handleLoadingState(index, true)
        var pattern = /^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/
        var isURL = pattern.test(this.state.inputsList[index].url)
        if(this.state.inputsList[index].url.length > 4){
            this.handleActiveState(index, true)
            if(!isURL){
                this.handlehasErrorState(index, true)
            }
            else {
                this.handlehasErrorState(index, false)
            }
        }
        else {
            this.handleActiveState(index, false)
        }
        this.handleLoadingState(index, false)
    }

    appendNewInput(){
        if(this.state.inputsList.length < 8){
            this.setState({
                inputsList: this.state.inputsList.concat(
                    {
                        index: this.state.inputsList.length,
                        url: '',
                        hasError: false,
                        active: false,
                        loading: false
                    }
                )
            })
        }
        else {
            Alert.alert(
                '',
                'You cannot add any more products',
              )
        }
    }

    handleURLChange(index, url){
        var inputs = [...this.state.inputsList];
        var id = inputs.findIndex(obj => obj.index === index);
        inputs[id].url = url;
        this.setState({
            inputsList: inputs
        });
    }

    onSwipeLeft(gestureState){
        var products = []
        this.state.inputsList.forEach(product => {
            products.push(product.url)
        })
        var errors = [
            validate('Products', products),
            validate('Name', this.state.name),
        ]
        this.props.sendErrors(errors)
        this.props.sendData({
            products: products,
            name: this.state.name,
            type: 'feed_products'
        })
    }

    showErrorMessage(errors){
        var messages = []
        errors.map((error, index) => {
            if(error !== null){
                messages.push(index !== errors.length - 1 ? `${error}, ` : `${error}.`)
            }
        })
        if(messages.length > 0){
            MessageBarManager.showAlert({
                title: '',
                message: messages,
                alertType: 'error',
                duration: 6000,
                viewTopInset: 5,
                viewBottomInset: 5,
                position: 'bottom',
            })
            this.props.disableSubmitButton()
        }
        else {
            this.props.enableSubmitButton()
        }
    }

    render(){
        return(
            <GestureRecognizer onSwipeLeft={(state) => this.onSwipeLeft(state)} style={styles.formContainer}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <KeyboardAwareScrollView 
                    style={[styles.scrollableView, {marginTop: 5, marginBottom: 5, flexGrow: 1}]}
                    enableOnAndroid={true} 
                    enableAutomaticScroll={(Platform.OS === 'ios')}
                >
                <Text style={styles.formLabel}>
                    Ad Name
                </Text>
                <View style={[formStyles.inputGroup, {flexDirection: 'row', marginTop: 2}]}>
                    <TextInput 
                        placeholder='Ad Name'
                        onChangeText={(name) => {
                            this.setState({name: name})                    
                        }}
                        underlineColorAndroid='transparent'
                        style={[styles.inputStyle, {marginTop: 10, marginBottom: 10}]}
                        placeholderTextColor='#999'>
                    </TextInput>
                </View>
                <Text style={styles.formLabel}>
                    Products URLs
                </Text>
                    {this.state.inputsList.map((input, index) => {
                        return(
                            <View style={[formStyles.inputGroup, styles.inputStyle ,{flexDirection: 'row'}]}>
                                <TextInput 
                                    placeholder='Product URL'
                                    onEndEditing={() => {
                                        this.searchHandler(index)
                                    }}
                                    onChangeText={(url) => {
                                        this.handleURLChange(index, url)
                                    }}
                                    underlineColorAndroid='transparent'
                                    placeholderTextColor='#999'
                                    style={{width: '90%'}}>
                                </TextInput>
                                {!input.loading ? <Ionicons style={styles.checkMark} 
                                    name={input.active ? (
                                        !input.hasError ? 'ios-checkmark-circle' : 'ios-information-circle'
                                    ): 'ios-information-circle'} 
                                    size={25} 
                                    color={input.active ? (
                                        !input.hasError ? '#4BB543' : '#dc3d4c'
                                    ): '#ddd'}/> :
                                <ActivityIndicator size="small" color="#ddd"/>
                                }
                            </View>
                        )
                    })}
                    </KeyboardAwareScrollView>
                <AddProduct
                    pressHandler={this.appendNewInput}
                    style={styles.actionButton} 
                    loaded={true}
                    innerStyle={styles.actionButtonInnerStyle}
                    underlayColor="#c61b40"
                    hasIcon={true}
                    icon={{iconName: 'md-add', size: 30, color: '#fff'}}
                />
            </GestureRecognizer>
        )
    }
}


const styles = StyleSheet.create({
    formContainer: {
        width: '100%',
        flex: 1
    },
    inputStyle: {
        padding: 12,
        paddingLeft: 12,
        borderRadius: 2,
        alignSelf: 'center',
        width: '90%',
        marginTop: 20,
        color: '#222',
        backgroundColor: '#fff',
        borderColor: '#999',
        borderWidth: .5
    },
    notFound: {
        borderColor: '#cc0000',
        borderWidth: .5
    },
    scrollableView: {
        flex: 1,
        paddingBottom: 10,
        marginBottom: 200
    },
    actionButton: {
        position: 'absolute',
        height: 65,
        width: 65,
        bottom: 20,
        right: 15,
        zIndex: 50,
        backgroundColor: '#ea1f4a',
        borderRadius: 100,
        shadowColor: "#222",
        shadowOffset: {
            width: 4,
            height: 2,
        },
        shadowOpacity: 0.18,
        shadowRadius: 2.46,
        elevation: 4,
    },
    actionButtonInnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    formLabel: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 18,
        fontSize: 15,
        fontWeight: 'bold',
        color: '#777'
    }
})