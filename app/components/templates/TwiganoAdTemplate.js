import React, {Component} from 'react'
import {View, Image, TouchableHighlight} from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import {Video, Linking} from 'expo'
import CallAction from './../GenericButton'
import {genericStyles} from './../../configs/styles/generic.styles'
import {buttonsStyles} from './../../configs/styles/buttons.styles'

export default class TwiganoAdTemplate extends Component {
    constructor(props){
        super(props)
        this.state = {
            content: {}   
        }
    }
    
    componentDidMount(){
        this.setState({
            content: this.props.content
        })
    }


    navigateToAction(type, data){
        if(type == 'get_directions'){
            Linking.openURL('http://maps.google.com/maps?daddr=' + this.props.coords)
        }
        else {
            Linking.openURL(data)
        }
    }

    render(){
        return(
            <View>
                {this.state.content ? 
                (this.state.content.media_type == 'image' ?
                    <Image style={genericStyles.mediaPlaceholder} source={{uri: this.state.content.media}}/>
                    : <Video style={genericStyles.mediaPlaceholder} 
                        resizeMode="cover"
                        shouldPlay
                        useNativeControls
                        isLooping source={{uri: this.state.content.media}}/>
                )
                : <TouchableHighlight style={[genericStyles.mediaPlaceholder, 
                    {backgroundColor: '#d1d1d1'}]}
                    underlayColor='transparent'>
                    <Ionicons name='md-images' size={55} color='#777'/>
                </TouchableHighlight>}
                <CallAction
                    pressHandler={() => {
                        this.navigateToAction(this.state.content.call_to_action, this.state.content.action_url)
                    }}
                    style={[buttonsStyles.actionButton, {height: '15%', backgroundColor: '#4a8bfc'}]}
                    innerStyle={buttonsStyles.actionButtonInnerStyle}
                    textStyle={[buttonsStyles.actionButtonText, {fontWeight: 'normal'}]}
                    title={this.props.actionTitle}
                    hasIcon={true}
                    loaded={true}
                    icon={{iconName: 'ios-arrow-dropright', size: 20, color: '#fff'}}
                />
            </View>
        )
    }
}