import React, {Component} from 'react'
import {View, ListView, ScrollView, Text, StyleSheet} from 'react-native'

export default class FeedProductsTemplate extends Component {
    constructor(props){
        super(props)
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            productsCount : 0,
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
        }
    }
    
    componentDidMount(){
        this.setState({
            content: this.props.content
        })
    }

    render(){
        return(
            <View>
                {
                    this.state.productsCount % 2 == 0 ? 
                        <ListView
                            style={styles.list}
                            dataSource={this.state.dataSource}
                            renderRow={(rowData) => <Text style={styles.item}>{rowData}</Text>}
                        />
                    :
                        <ScrollView horizontal={true}>

                        </ScrollView>
                }           
            </View>
        )
    }

}

var styles = StyleSheet.create({
    list: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    item: {
        backgroundColor: '#CCC',
        margin: 10,
        width: 100,
        height: 100
    }
});