import React, {Component} from 'react'
import {Image, View, StyleSheet, Text, TextInput, TouchableOpacity, ImageStore, ImageEditor} from 'react-native'
import {ImageManipulator} from 'expo'
import { Ionicons, Octicons } from '@expo/vector-icons'
import { formStyles } from '../configs/styles/form.styles'
import { genericStyles } from '../configs/styles/generic.styles'
import { buttonsStyles } from '../configs/styles/buttons.styles'
import GenericButton from './../components/GenericButton'
import { networkHandler } from '../configs/NetworkHandler'
import store from 'react-native-simple-store'

export default class UpdatePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            new_password: '',
            name: '',
            email: '',
            username: '',
            biography: '',
            gender: '',
            birthday: '',
            longitude: '',
            latitude: '',
            avatar: '',
            base64: ''
        }
    }

    static navigationOptions = {
        title: 'Change Password',
        headerTitleStyle: {
            textAlign: 'center',
            alignSelf:'center',
        },
        tabBarVisible: false, 
        swipeEnabled: false, 
        headerRight: (<View></View>)
    }

    componentWillMount(){
        store.get('user').then((user) => {
            this.setState({
                name: user.name,
                email: user.email,
                username: user.username,
                biography: user.biography,
                gender: user.gender,
                birthday: user.birthday,
                longitude: user.longitude,
                latitude: user.latitude,
                avatar: user.avatar
            }, () => {
                this.convertToBase64()
            })
        })
    }

    convertToBase64 = async () => {
        const manipResult = await ImageManipulator.manipulate( this.state.avatar,
          [{ rotate: 0}, { flip: { vertical: true }}],
          { format: 'png', base64: true }
        );
        this.setState({
            base64: manipResult.base64
        })
    }


    handleSubmit(){
        var body = {
            new_password: this.state.new_password,
            password: this.state.password,
            name: this.state.name,
            email: this.state.email,
            username: this.state.username,
            biography: this.state.biography,
            gender: this.state.gender ? this.state.gender : 'other',
            birthday: this.state.birthday,
            longitude: this.state.longitude,
            latitude: this.state.latitude,
            avatar: this.state.base64
            
        }
        store.get('access_token').then((token) => {
            networkHandler.updateAccount(body, token, (data) => {
                alert(JSON.stringify(data))
            })
        })
    }
    
    render(){
        return(
            <View style={{height: '100%'}}>
                <View style={genericStyles.section}>
                    <Text style={genericStyles.sectionHeader}>Change Password</Text>
                    <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                        <TextInput 
                            placeholder='Current Password'
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            onChangeText={(password) => this.setState({password})}
                            secureTextEntry
                            style={{width: '90%', paddingLeft: 5}}>
                        </TextInput>
                        <TouchableOpacity onPress={() => {
                            this.handleInputVisibility('current_password')    
                        }}>
                            <Ionicons
                                name='ios-eye' 
                                size={28} 
                                color='#ddd'
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={[formStyles.sectionGroup, formStyles.inputStyle, formStyles.withoutBorder]}>
                        <TextInput 
                            placeholder='New Password'
                            underlineColorAndroid='transparent'
                            secureTextEntry
                            onChangeText={(new_password) => this.setState({new_password})}
                            placeholderTextColor='#999'
                            style={{width: '90%', paddingLeft: 5}}>
                        </TextInput>
                        <TouchableOpacity onPress={() => {
                            this.handleInputVisibility('new_password')    
                        }}>
                            <Ionicons
                                name='ios-eye' 
                                size={28} 
                                color='#ddd'
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <GenericButton
                    pressHandler={() => {
                        this.handleSubmit()
                    }}
                    style={[buttonsStyles.actionButton, {height: '8%'}]} 
                    innerStyle={buttonsStyles.actionButtonInnerStyle}
                    textStyle={buttonsStyles.actionButtonText}
                    underlayColor="#008263"
                    title='Save'
                    loaded={true}
                    hasIcon={true}
                    icon={{iconName: 'ios-checkmark', size: 30, color: '#fff'}}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10
    },
    instructions: {
      fontSize: 12,
      textAlign: 'center',
      color: '#888',
      marginBottom: 5
    },
    cardInfoItem: {
        width: '80%', 
        paddingLeft: 15,
        marginTop: 5,
        fontSize: 15,
        color: '#666',
        fontWeight: 'bold'
    }
  })