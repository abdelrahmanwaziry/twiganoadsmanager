import React, {Component} from 'react'
import {View, TextInput, StyleSheet, Alert, Platform, Linking} from 'react-native'
import InputScrollView from 'react-native-input-scroll-view'
import { Picker } from 'react-native-picker-dropdown'
import GenericButton from './GenericButton'
import { Location, Permissions, IntentLauncherAndroid  } from 'expo'
import store from 'react-native-simple-store'
import { buttonsStyles } from '../configs/styles/buttons.styles'
import { networkHandler } from '../configs/NetworkHandler'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'

export default class BusinessAccountForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            loaded: true,
            first_name: '',
            last_name: '',
            business_name: '',
            business_type: 'store',
            website_url: '',
            business_email: '',
            number_of_products: 0,
            number_of_views: 0,
            phone_number: '',
            country: '',
            items_count: 0,
            postal_code: '',
            location: null,
            address: {},
        }
    }

    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
        this._getLocationAsync();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.data){
            this.setState({
                first_name: nextProps.data.first_name && nextProps.data.first_name,
                last_name: nextProps.data.last_name && nextProps.data.last_name,
                business_name: nextProps.data.business_name && nextProps.data.business_name,
                business_type: nextProps.data.type && nextProps.data.type,
                website_url: nextProps.data.url && nextProps.data.url,
                business_email: nextProps.data.email && nextProps.data.email,
                phone_number: nextProps.data.phone && nextProps.data.phone,
                number_of_products: nextProps.data.items_count && nextProps.data.items_count,
                number_of_views: nextProps.data.items_count && nextProps.data.items_count,
                postal_code: nextProps.data.address.postal_code && nextProps.data.address.postal_code
            })
        }
    }

    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }

    _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
          this.setState({
            errorMessage: 'Permission to access location was denied',
          });
        }
        try {
            let location = await Location.getCurrentPositionAsync({
                enableHighAccuracy: true
            })
            this.setState({ 
                location 
            }, () => {
                this._attemptReverseGeocodeAsync()
            })
        } catch (error) {
            Alert.alert(
                'Location Request',
                'We need to get your location to complete your business account setup',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Enable', onPress: () => 
                        Platform.OS == 'ios' ? 
                        Linking.openURL('App-Prefs:root=Privacy&path=LOCATION')
                        : IntentLauncherAndroid.startActivityAsync(
                            IntentLauncherAndroid.ACTION_LOCATION_SOURCE_SETTINGS,
                        ).then(() => {
                            this._getLocationAsync()
                        })
                    },
                ],
                { cancelable: false }
            )
        }
    };

    _attemptReverseGeocodeAsync = async () => {
        try {
            let result = await Location.reverseGeocodeAsync(this.state.location.coords)
            this.setState({
                address: result[0]
            })
        } catch (error) {
            console.log(error)
        }
    } 

    formatePhoneNumber(number){
        return `+2${number[0]}-${number[1]}${number[2]}${number[3]}-${number[4]}${number[5]}${number[6]}-${number[7]}${number[8]}${number[9]}${number[10]}`
    }

    handleSubmit(){
        this.setState({loaded: false})
        var body = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            business_name: this.state.business_name,
            type: this.state.business_type,
            url: this.state.website_url,
            email: this.state.business_email,
            items_count: this.state.business_type == 'store' ? this.state.number_of_products 
                : this.state.number_of_views,
            phone: this.state.phone_number,
            address: {
                street: this.state.address.street,
                building: '10',
                floor: '1',
                apartment: '1',
                city: this.state.address.city ? this.state.address.city : this.state.address.region,
                state: this.state.address.region,
                country: this.state.address.isoCountryCode,
                postal_code: this.state.postal_code
            }
        }

        store.get('access_token').then(token => {
            networkHandler.createBusinessAccount(body, token, (data) => {
                this.setState({loaded: true})
                this.props.data ? this.props.navigateToHome() : this.props.navigation.navigate('App')
            })
        })
    }

    render(){
        return(
            <View style={styles.container}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                <InputScrollView showsVerticalScrollIndicator={true} 
                    useAnimatedScrollView={true} style={styles.scrollableView}>
                    <TextInput 
                        value={this.state.first_name}
                        placeholder='First Name'
                        onChangeText={(first_name) => this.setState({first_name})}
                        underlineColorAndroid='transparent'
                        style={styles.cardInner}>
                    </TextInput>
                    <TextInput
                        value={this.state.last_name} 
                        placeholder='Last Name'
                        underlineColorAndroid='transparent'
                        onChangeText={(last_name) => this.setState({last_name})}
                        style={styles.cardInner}>
                    </TextInput>
                    <TextInput
                        value={this.state.business_name} 
                        placeholder='Business Name'
                        underlineColorAndroid='transparent'
                        onChangeText={(business_name) => this.setState({business_name})}
                        style={styles.cardInner}>
                    </TextInput>
                    <Picker
                        selectedValue={this.state.business_type}
                        style={styles.pickerInputStyle}
                        mode='dropdown'
                        onValueChange={(itemValue, itemIndex) => this.setState({business_type: itemValue})}>
                        <Picker.Item style={styles.pickerItemStyle} label="Store" value="store" />
                        <Picker.Item style={styles.pickerItemStyle} label="Publisher" value="publisher" />
                    </Picker>
                    {this.state.business_type == 'store' ?
                    <TextInput
                        value={this.state.number_of_products.toString()}
                        placeholder='Esitmated products amount'
                        keyboardType='numeric'
                        underlineColorAndroid='transparent'
                        onChangeText={(number_of_products) => this.setState({number_of_products})}
                        style={styles.cardInner}>
                    </TextInput>  
                    : <TextInput
                        value={this.state.number_of_views.toString()}
                        placeholder='Esitmated views amount'
                        keyboardType='numeric'
                        underlineColorAndroid='transparent'
                        onChangeText={(number_of_views) => this.setState({number_of_views})}
                        style={styles.cardInner}>
                    </TextInput>}
                    <TextInput 
                        value={this.state.website_url}
                        placeholder='Business URL'
                        underlineColorAndroid='transparent'
                        onChangeText={(website_url) => this.setState({website_url})}
                        style={styles.cardInner}>
                    </TextInput>
                    <TextInput 
                        value={this.state.business_email}
                        placeholder='Business Email'
                        keyboardType='email-address'
                        underlineColorAndroid='transparent'
                        onChangeText={(business_email) => this.setState({business_email})}
                        style={styles.cardInner}>
                    </TextInput>
                    <TextInput 
                        value={this.state.phone_number}
                        placeholder='Phone'
                        keyboardType='phone-pad'
                        underlineColorAndroid='transparent'
                        onChangeText={(phone_number) => this.setState({phone_number})}
                        style={styles.cardInner}>
                    </TextInput>
                    <TextInput
                        value={this.state.postal_code} 
                        placeholder='Postal Code'
                        keyboardType='numeric'
                        underlineColorAndroid='transparent'
                        onChangeText={(postal_code) => this.setState({postal_code})}
                        style={styles.cardInner}>
                    </TextInput>
                </InputScrollView>
                <GenericButton
                    loaded={this.state.loaded}
                    pressHandler={() => {
                        if(this.state.location){
                            this.handleSubmit()
                        }
                        else {
                            MessageBarManager.showAlert({
                                title: '',
                                message: 'Location is required',
                                alertType: 'error',
                                position: 'bottom',
                            }); 
                        }
                    }}
                    style={[buttonsStyles.placeOrderButton, {
                        backgroundColor: this.state.location ? '#d80746' : '#d092a5',
                        position: 'absolute',
                        bottom: 30
                    }]}
                    hasIcon={false}
                    innerStyle={[buttonsStyles.actionButtonInnerStyle, buttonsStyles.placeOrderButtonInnerStyle]}
                    textStyle={[buttonsStyles.actionButtonText, {fontWeight: 'normal'}]}
                    title='Submit'
                    underlayColor={this.state.location ? "#d80746" : '#d80746'}
                />
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        flex: 0,
        backgroundColor: '#fff'
    },
    scrollableView: {
        flex: 0,
        height: 450,
        marginBottom: 100,
    },
    image: {
        alignSelf: 'center',
        marginTop: 100,
        marginBottom: 50,
        width: 100,
        height: 100,
        resizeMode: 'contain'
    },
    cardInner : {
        padding: 10,
        paddingLeft: 20,
        marginTop: 20,
        fontSize: 15,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderWidth: 1.5,
        borderColor: '#d2d1d1',
        borderRadius: 5,
        color: '#777'
    },
    pickerInputStyle: {
        padding: Platform.OS == 'ios' ? 5 : 2,
        paddingLeft: 20,
        marginTop: 20,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderWidth: 1.5,
        borderColor: '#d2d1d1',
        borderRadius: 5,
    },
    pickerItemStyle: {
        width: '100%',
        padding: 5
    },
    buttonStyle: {
        padding: 10,
        marginTop: 10,
        marginBottom: 10,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#d80746',
        borderRadius: 5
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold'
    }
})