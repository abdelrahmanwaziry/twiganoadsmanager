import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import { Ionicons} from '@expo/vector-icons'

export default class EmptyState extends Component {
    render(){
        return(
            <View style={styles.container}>
                <Ionicons name='md-outlet' size={55} color='#aaa'/>
                <Text style={{fontSize: 20, color: '#999'}}>No data found</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '80%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'column',
    }
})