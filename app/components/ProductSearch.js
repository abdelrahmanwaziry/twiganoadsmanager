import React, { Component } from 'react' 
import { 
    View, 
    Text, 
    TextInput, 
    StyleSheet, 
    TouchableHighlight, 
    Platform, 
    Alert, 
    ScrollView 
} from 'react-native'
import GenericButton from './../components/GenericButton'
import { Ionicons, Octicons } from '@expo/vector-icons'
import { networkHandler } from '../configs/NetworkHandler'
import store from 'react-native-simple-store'
import { formStyles } from '../configs/styles/form.styles';
import { genericStyles } from '../configs/styles/generic.styles';
import { buttonsStyles } from '../configs/styles/buttons.styles';

export default class FeedProductsContentForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            keyword: '',
            editEnded: false
        }
        this.searchHandler = this.searchHandler.bind(this)
    }

    componentDidMount(){
    }

    searchHandler(){
        store.get('access_token').then((token) => 
            networkHandler.productSearch(this.state.keyword, token, (data) => {
                Alert.alert('', JSON.stringify(data))
            })
        )
    }

    render(){
        return(
            <View style={genericStyles.section}>
                <View style={[formStyles.sectionGroup, {flexDirection: 'row'}]}>
                    <TextInput 
                        placeholder='Search..'
                        onEndEditing={() => {
                            this.setState({editEnded: true})
                        }}
                        onChangeText={(keyword) => this.setState({keyword})}
                        underlineColorAndroid='transparent'
                        placeholderTextColor='#999'
                        style={[formStyles.inputStyle, formStyles.withoutBorder ,{width: '75%'}]}>
                    </TextInput>
                    <GenericButton
                        disabled={false}
                        pressHandler={this.searchHandler}
                        hasIcon={true}
                        underlayColor='transparent'
                        innerStyle={buttonsStyles.addButtonInnerStyle}
                        style={buttonsStyles.searchButtonStyle}
                        icon={{iconName: 'ios-search', size: 25, color: '#ddd'}}
                    />
                </View>
            </View>
        )
    }
}
