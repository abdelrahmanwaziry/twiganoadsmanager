import React, {Component} from 'react'
import {View, Platform} from 'react-native'
import InputScrollView from 'react-native-input-scroll-view'

export default class InputScrollViewWrapper extends Component {
    render(){
        return(
            Platform.OS == 'ios' ? <InputScrollView>
            </InputScrollView> : <InputScrollView></InputScrollView>
        )
    }
}