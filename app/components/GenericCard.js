import React, {Component} from 'react'
import {View, Text, Image} from 'react-native'
import {genericStyles} from './../configs/styles/generic.styles'
import { Col, Row, Grid } from "react-native-easy-grid"
import { Entypo, Octicons } from '@expo/vector-icons' 

export default class GenericCard extends Component {
    render(){
        return(
            <View style={[genericStyles.cardStyle, {width: '90%', marginTop: 5, marginBottom: 5}]}>
                <View style={genericStyles.cardHeader}>
                    <View style={genericStyles.cardHeaderIcon}>
                        <Octicons name='megaphone' size={25} color='#fff'/>
                    </View>
                    <Text style={genericStyles.cardHeaderText}>
                        {this.props.data.name}
                    </Text>
                </View>
                <Image style={[genericStyles.mediaPlaceholder, {height: 120}]} 
                resizeMode='cover' source={{uri: this.props.data.media}}/>
                <View style={genericStyles.cardFooter}>
                    <Row>
                        <Col>
                            <Text style={genericStyles.cardHeaderText}>
                                {'Duration: ' + this.props.data.duration + ' days'}
                            </Text>
                        </Col>
                        <Col>
                            <Text style={genericStyles.cardFooterText}>
                                {'Bid amount: ' + '$' + this.props.data.bid}
                            </Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Text style={[genericStyles.cardFooterText, {
                                    color: '#999',
                                    textAlign: 'left',
                                    paddingVertical: 5
                                }]}>
                                {this.props.data.status === 'pending' ? 'Banner is being reviewed' : 'Running'}
                            </Text>
                        </Col>
                    </Row>
                </View>
            </View>
        )
    }
}