import React, { Component } from 'react' 
import { View, TouchableHighlight, Text, ActivityIndicator, Platform } from 'react-native'
import { Ionicons, Octicons } from '@expo/vector-icons'

export default class GenericButton extends Component {
    render(){
        return(
            <TouchableHighlight
                underlayColor={this.props.underlayColor}
                onPress={!this.props.disabled ? this.props.pressHandler : () => {
                    return false
                }}
                style={this.props.style}
            >
                <View style={this.props.innerStyle}>
                    {this.props.loaded ? 
                    <Text style={this.props.textStyle}>
                        {this.props.title}
                    </Text> : 
                    <ActivityIndicator size="small" color="#f2f2f2"/>
                    }
                    {this.props.loaded && this.props.hasIcon && <Ionicons 
                        name={this.props.icon.iconName} 
                        size={this.props.icon.size} 
                        color={this.props.icon.color}
                    />}
                </View>                
            </TouchableHighlight>
        )
    }
}

