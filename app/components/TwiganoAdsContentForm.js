import React, { Component } from 'react' 
import {View, 
        StyleSheet, 
        TouchableHighlight, 
        TextInput, 
        Text, 
        Image,
        Alert,
        Platform} from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import GestureRecognizer from 'react-native-swipe-gestures'
import { ImagePicker, Video } from 'expo'
import { Picker } from 'react-native-picker-dropdown'
import { formStyles } from '../configs/styles/form.styles';
import validate from '../configs/ValidationHandler'
import {MessageBar as MessageBarAlert} from 'react-native-message-bar'
import {MessageBarManager} from 'react-native-message-bar'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class TwiganoAdsContentForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            media: '',
            name: '',
            media_type: 'image',
            link: '',
            mapViewVisible: false,
            location: '',
            action_type: 'get_directions',
            message: '',
        }
        this.pickImage = this.pickImage.bind(this)
        this.cameraPermission = this.cameraPermission.bind(this)
    }
    
    componentDidMount(){
        MessageBarManager.registerMessageBar(this.errorMessage);
    }

    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }

    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
          mediaTypes: 'All'
        });
        if (!result.cancelled) {
            this.setState({ 
              media: result.uri,
              media_type: result.type
            });
        }
    }

    cameraPermission = async () => {
        if(Platform.OS == 'ios'){
            const { Location, Permissions } = Expo;
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status === 'granted') {
                this.pickImage()
            } else {
                Alert.alert('Permission Denied', 'You need to allow Camera Roll Permission to pick an Image')
            }
        }
        else {
            this.pickImage()
        }
    }

    showErrorMessage(errors){
        var messages = []
        errors.map((error, index) => {
            if(error !== null){
                messages.push(index !== errors.length - 1 ? `${error}, ` : `${error}.`)
            }
        })
        if(messages.length > 0){
            MessageBarManager.showAlert({
                title: '',
                message: messages,
                alertType: 'error',
                duration: 6000,
                viewTopInset: 5,
                viewBottomInset: 5,
                position: 'bottom',
            })
            this.props.disableSubmitButton()
        }
        else {
            this.props.enableSubmitButton()
        }
    }

    onSwipeLeft(gestureState){
        var errorMessages = [
            validate('Message', this.state.message),
            validate('Name', this.state.name),
            validate('Action URL', this.state.link),
            validate('Media', this.state.media)
        ]
        this.props.sendErrors(errorMessages)
        this.props.sendData({
            media: this.state.media,
            media_type: this.state.media_type,
            call_to_action: this.state.action_type,
            caption: this.state.message,
            type: 'media',
            name: this.state.name,
            action_url: this.state.link,
        })
    }

    render(){
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        }
        return(
            <GestureRecognizer onSwipeLeft={(state) => this.onSwipeLeft(state)} style={styles.formContainer}>
                <MessageBarAlert ref={(errorMessage) => { this.errorMessage = errorMessage; }}/>
                {this.state.media ? 
                <TouchableHighlight underlayColor='transparent' 
                    onPress={this.state.media_type == 'image' && this.cameraPermission}
                    onLongPress={this.state.media_type == 'video' && this.cameraPermission}
                    > 
                    {this.state.media_type == 'image' ?
                    <Image style={styles.mediaPlaceholder} source={{uri: this.state.media}}/>
                    : <Video style={styles.mediaPlaceholder} 
                        resizeMode="cover"
                        shouldPlay
                        useNativeControls
                        isLooping source={{uri: this.state.media}}/>
                    }
                </TouchableHighlight>
                : <TouchableHighlight style={[styles.mediaPlaceholder, 
                    {backgroundColor: '#d1d1d1'}]}
                    underlayColor='transparent'
                    onPress={this.cameraPermission}>
                    <Ionicons name='ios-images-outline' size={55} color='#777'/>
                </TouchableHighlight>}
                <KeyboardAwareScrollView 
                    style={{marginTop: 5, marginBottom: 5, flexGrow: 1}}
                    enableOnAndroid={true} 
                    enableAutomaticScroll={(Platform.OS === 'ios')}
                >
                    <View style={styles.inputGroup}>
                        <Text style={styles.inputGroupLabel}>
                            Call to Action
                        </Text>
                        <Picker
                            selectedValue={this.state.action_type}
                            style={[styles.inputStyle, styles.pickerInputStyle]}
                            mode='dropdown'
                            onValueChange={(itemValue, itemIndex) => this.setState({action_type: itemValue})}>
                            <Picker.Item label="Get Directions" value="get_directions" />
                            <Picker.Item label="Visit Website" value="visit_website" />
                            <Picker.Item label="Shop Now" value="shop_now" />
                            <Picker.Item label="Read More" value="read_more" />
                        </Picker>
                        {this.state.action_type !== 'get_directions' ?
                            <TextInput
                                placeholder='Content URL'
                                onChangeText={(link) => {
                                    this.setState({link})
                                }}
                                underlineColorAndroid='transparent'
                                placeholderTextColor='#999'
                                style={[styles.inputStyle]}>
                            </TextInput> :
                            <TouchableHighlight 
                                underlayColor='transparent'
                                onPress={this.props.navigateToMap}
                                style={[formStyles.inputStyle, {backgroundColor: '#ededed', width: '90%'}]}>
                                <Text>
                                    {this.props.currentLocation}
                                </Text>
                            </TouchableHighlight>
                        }
                    </View>
                    <View style={styles.inputGroup}>
                        <Text style={styles.inputGroupLabel}>
                            Ad Name
                        </Text>
                        <TextInput 
                            placeholder='Enter ad name here'
                            onChangeText={(name) => this.setState({name})}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            style={[styles.inputStyle]}>
                        </TextInput>
                    </View>
                    <View style={styles.inputGroup}>
                        <Text style={styles.inputGroupLabel}>
                            Ad Message
                        </Text>
                        <TextInput 
                            placeholder='Enter ad message here'
                            onChangeText={(message) => this.setState({message})}
                            underlineColorAndroid='transparent'
                            placeholderTextColor='#999'
                            style={[styles.inputStyle]}>
                        </TextInput>
                    </View>
                </KeyboardAwareScrollView>
            </GestureRecognizer>
        )
    }
}

const styles = StyleSheet.create({
    formContainer: {
        width: '100%',
        height: '100%',
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'column',
    },
    mediaPlaceholder: {
        width: '100%',
        height: 170,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap', 
        flexDirection:'row',
    },
    inputGroup: {
        flexWrap: 'wrap', 
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 5
    },
    inputGroupLabel: {
        marginTop: 10,
        width: '90%',
        alignSelf: 'center',
        fontSize: 15,
        fontWeight: 'bold',
        color: '#aaa'
    },
    inputStyle: {
        padding: Platform.OS == 'ios' ? 20 : 10,
        paddingLeft: 10,
        borderRadius: 2,
        alignSelf: 'center',
        width: '90%',
        marginRight: 5,
        marginTop: 10,
        borderColor: '#d8d8d8',
        borderWidth: .5
    },
    pickerInputStyle: {
        padding: Platform.OS == 'ios' ? 15 : 2,
        backgroundColor: '#f0f0f0'
    },
})